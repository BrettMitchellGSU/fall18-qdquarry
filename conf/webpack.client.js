const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');

module.exports = function(env) { 
    const base = require( './webpack.base' )(env);
    
    var conf = {};
    
    Object.assign(conf, base, {

        entry: [
            path.resolve(__dirname, '..', 'src', 'client', env.target, 'index.jsx'),
            path.resolve(__dirname, '..', 'src', 'client', 'scss', 'index.' + env.target + '.scss')
        ],
        output: {
            filename: 'bundle.client.' + env.target + '.js',
            path: path.resolve(__dirname, '..', 'src', 'server', 'content-service', 'static', 'bundle')
        },

        plugins: [
            ...base.plugins,
            new MiniCssExtractPlugin({
                filename: 'bundle.client.' + env.target + '.css',
                chunkFilename: '[id].css'
            })
        ],

        // When importing a module whose path matches one of the following, just
        // assume a corresponding global variable exists and use that instead.
        // This is important because it allows us to avoid bundling all of our
        // dependencies, which allows browsers to cache those libraries between
        // builds. 
        externals: {
            'react': 'React',
            'react-dom': 'ReactDOM',
            'react-data-grid': 'ReactDataGrid',
            'mammoth': 'mammoth',
            'xlsx': 'XLSX',
            'axios': 'axios',
            'react-visibility-sensor': 'react-visibility-sensor',
            '@blueprintjs/core': 'Blueprint.Core',
            'react-redux': 'ReactRedux',
            'redux': 'Redux',
            'three': 'THREE'
        }
    });

    conf.module.rules = conf.module.rules.concat([
        {
            test: /\.s?css$/,
            use: [
                { loader: MiniCssExtractPlugin.loader },
                { loader: 'css-loader', options: { minimize: true } },
                { loader: 'sass-loader' }
            ],
            exclude: /node_modules/
        }
    ]);

    return conf;

};