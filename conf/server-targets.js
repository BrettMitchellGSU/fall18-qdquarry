
module.exports = [
    /*{
        opts: ['account-service', 'account', 'ac'],
        realTarget: 'account-service'
    },
    {
        opts: ['auth-service', 'auth', 'au'],
        realTarget: 'auth-service'
    },*/
    {
        opts: ['code-service', 'code', 'c'],
        realTarget: 'code-service'
    },
    {
        opts: ['file-service', 'file', 'f'],
        realTarget: 'file-service'
    },
    {
        opts: ['hierarchy-service', 'hierarchy', 'h'],
        realTarget: 'hierarchy-service'
    },
    {
        opts: ['worskpace-service', 'workspace', 'w'],
        realTarget: 'workspace-service'
    },
    {
        opts: ['content-service', 'content', 'con'],
        realTarget: 'content-service'
    }
];
