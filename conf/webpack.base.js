const path = require('path');

const MinifyPlugin = require('babel-minify-webpack-plugin');

module.exports = function(env) {
    return {
        mode: 'none',

        // Enable sourcemaps for debugging webpack's output.
        devtool: env.mode === 'production' ? false : 'source-map',

        module: {
            rules: [
                { test: /\jsx?$/,
                  exclude: /node_modules/,
                  loader: 'babel-loader'   }
            ]
        },

        resolve: {
            extensions: ['.js', '.jsx', '.scss', '.css', '.json']
        },

        plugins: env.mode === 'production' ?
                    [ new MinifyPlugin() ] :
                    []
    };
};
