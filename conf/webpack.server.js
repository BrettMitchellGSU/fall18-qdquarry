
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { readdirSync, statSync } = require('fs');
const { join, resolve } = require('path');
const nodeExternals = require('webpack-node-externals');

function getDirs(p) {
    return readdirSync( p ).filter( f => statSync(join(p, f)).isDirectory() );
}

const targets = require('./server-targets');

function serializeOpts() {
    let s = '';
    targets.forEach(t => {
        let _s = '';
        t.opts.forEach(o => _s += o);
        s += _s;
    });
    return s;
}

module.exports = function(env) {
    let base = require( './webpack.base' )(env);

    let target = null;

    targets.some(t => {
        if (t.opts.includes(env.target)) {
            target = t.realTarget;
            return true; // found a target matching an argument
        }
        return false; // continue searching for a matching target
    });

    if (target) {
        let conf = Object.assign({}, base, {
            entry: resolve(__dirname, '..', 'src', 'server', target, 'index.js'),
            output: {
                filename: 'bundle.server.' + target + '.js',
                path: resolve(__dirname, '..', 'output', 'bundle')
            },

            target: 'node',

            externals: [
                nodeExternals()
            ],

            node: {
                __dirname: false,
                __filename: false,
                fs: 'empty'
            }
        });

        return conf;
    }

    return 'No server target found. Please try one of the following:\n' +
            serializeOpts();
};
