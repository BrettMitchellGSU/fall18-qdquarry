// jshint ignore: start

import * as React from 'react';
import ReactDOM from 'react-dom';

import { Login } from './js/login/Login';

ReactDOM.render(<Login/>, document.getElementById('root'));