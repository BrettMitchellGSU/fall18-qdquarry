// jshint ignore: start

import * as React from 'react';
import ReactDOM from 'react-dom';

import { ContentScaffold } from './js/common/ContentScaffold';

import { createStore } from 'redux';

ReactDOM.render(<ContentScaffold/>, document.getElementById('root'));