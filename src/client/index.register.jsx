// jshint ignore: start

import * as React from 'react';
import ReactDOM from 'react-dom';

import { Register } from './js/register/Register';

ReactDOM.render(<Register/>, document.getElementById('root'));