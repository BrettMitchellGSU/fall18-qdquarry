import Axios from "axios";

const mode = 'development';

const prefix = mode === 'production' ? 'http://api.qdquarry.com' : 'http://localhost:8080';

const methods = {
    'a': {
        'code': {
            'add-segment':    ['codeID', 'workspaceID', 'fileID', 'fileName', 'contents', 'offset'],
            'remove-segment': ['codeID', 'workspaceID', 'segmentIdx'],
            'add-tag':        ['codeID', 'workspaceID', 'tag'],
            'remove-tag':     ['codeID', 'workspaceID', 'tag'],
            'change-color':   ['codeID', 'workspaceID', 'color']
        },
        'hierarchy': {
            'get-hierarchy': ['scope', 'scopeID', 'type'],
            'create-item':   ['scope', 'scopeID', 'type', 'itemPath', 'itemName', 'itemType', 'itemData'],
            'delete-item':   ['scope', 'scopeID', 'type', 'itemPath', 'itemName'],
            'rename-item':   ['scope', 'scopeID', 'type', 'itemPath', 'oldItemName', 'newItemName'],
            'move-item':     ['scope', 'scopeID', 'type', 'oldItemPath', 'newItemPath', 'itemName']
        }
    },
    'ua': {}
};

function findMethodDef(method) {
    let splitMethod = method.split('/').map(e => e.trim()).filter(e => e.length);
    let element;
    let next = methods;
    while ((element = splitMethod.shift()) !== undefined) {
        next = next[element];
        if (!next)
            return null;
    }
    return next;
}

export const API = function(method, params) {
    const url = prefix + method;
    const methodDef = findMethodDef(method);
    if (!methodDef) throw 'No method found for API string ' + method;
    Object.keys(params).forEach(
        p => { if (!methodDef.includes(p)) throw 'Missing parameter ' + p; }
    );

    return Axios({url: url, method: 'POST', data: params});
};
