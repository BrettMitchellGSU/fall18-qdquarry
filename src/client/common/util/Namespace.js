
/* jshint ignore:start */

import uuidv4 from 'uuid/v4';

export const NamespaceActions = function(actions) {
    const namespaceUUID = uuidv4();
    let actionMap = actions.map(la => {
        return la.map(a => { return {[a]: namespaceUUID + a} })
    });
    actionMap = actionMap.map(a => a.reduce((built, cur) => { return {...built, ...cur} }))
                         .reduce((built, cur) => { return { ...built, ...cur } } );
    return {
        namespaceUUID: namespaceUUID,
        ...actionMap
    }
};
