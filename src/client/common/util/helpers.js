
export const getURLParameter = function(pathIndex) {
    return window.location.pathname.split('/').filter(p => p.length)[pathIndex];
};

export const getWorkspaceIDFromURLParameter = function() {
    return getURLParameter(1);
}
