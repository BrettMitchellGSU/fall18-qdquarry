
/* jshint ignore:start */

export const Permute = function(...actions) {
    return actions.map(a => [a, a + '_PENDING', a + '_REJECTED', a + '_FULFILLED']
                            .reduce((p, c) => { return Object.assign({}, p, {[c]: c}); }, {})
                        ).reduce((p, c) => { return Object.assign({}, p, c)}, {});
};
