
/* jshint ignore:start */

import * as React from 'react';
import * as bpcore from '@blueprintjs/core';

export class LoadingOverlay extends React.Component {
    render() {
        const { pageIsLoaded } = this.props;
        const visibility = (pageIsLoaded ? 'hidden' : 'visible')
        return (
            <div className={"qdquarry__loading-overlay qdquarry__loading-overlay--" + visibility + " color-site-background-medium "}>
                <bpcore.Spinner
                    className="qdquarry__loading-overlay-spinner"
                    intent={bpcore.Intent.PRIMARY}
                    value={pageIsLoaded ? 1 : null}>
                    
                </bpcore.Spinner>
            </div>
        )
    }
}
