/* jshint ignore:start */

import * as React from 'react';
import { connect } from 'react-redux';

import { LoadingOverlay } from './LoadingOverlay';
import { TitleBar } from './TitleBar';
import { ActionBar } from './ActionBar';
import { ListView } from '../../workspace/components/list-view/ListView';
import { MainPanel } from './MainPanel';

import * as bpcore from '@blueprintjs/core';

function PropsFromStore(store) {
    let pageIsLoaded = true;
    for (const h in store.hierarchies)
        pageIsLoaded = pageIsLoaded &&
                      !store.hierarchies[h].fetching &&
                       store.hierarchies[h].hierarchy !== null;
        
    return {
        pageIsLoaded: pageIsLoaded
    };
}

class contentScaffold extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listViewWidth: 250
        }
    }

    setListViewWidth = (newWidth) => {
        this.setState({listViewWidth: newWidth});
    }

    getToolFromMap = (map) => (tool) => {
        return map[tool];
    }

    render() {
        // workspace | dashboard
        let mode = this.props.mode;

        const modeSpecificInitialData = (mode === 'workspace' ? 
            [ { name: 'Files', data: [] }, { name: 'Codes',      data: [] } ] :
            [ { name: 'Files', data: [] }, { name: 'Workspaces', data: [] } ]);

        return (
            <div className="qdquarry__content-scaffold-wrapper">
                <LoadingOverlay pageIsLoaded={this.props.pageIsLoaded} />

                <div style={{zIndex: 4}} className="qdquarry__content-scaffold-title-bar">
                    <TitleBar pageLabel={this.props.pageLabel} />
                </div>
                <div style={{zIndex: 3}} className="qdquarry__content-scaffold-action-bar">
                    <ActionBar />
                </div>
                <div className="qdquarry__content-scaffold-main-content">
                    <div 
                        style={{width: this.state.listViewWidth + "px",
                                zIndex: 2}}
                        className="qdquarry__content-scaffold-list-view">

                        <ListView
                            placeholderData={modeSpecificInitialData}
                            listViewButtonSet={this.props.listViewButtonSet}
                            setOwnWidth={this.setListViewWidth.bind(this)}
                            width={this.state.listViewWidth}
                            minWidth={175} />
                    </div>
                    <div
                        style={{width: "calc(100vw - " + this.state.listViewWidth + "px)",
                                zIndex: 1}}
                        className="qdquarry__content-scaffold-main-panel">
                        <MainPanel getTool={this.getToolFromMap(this.props.toolMap)} />
                    </div>
                </div>
            </div>
        );
    }
};

export const ContentScaffold = connect(PropsFromStore)(contentScaffold);
