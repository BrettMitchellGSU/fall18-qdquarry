/* jshint ignore:start */

import * as React from 'react';
import * as bpcore from '@blueprintjs/core';

export class ActionBarButton extends React.Component {

}

export class ActionBar extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            buttons: props.buttons,
            slideOutOptions: [],
            isOpen: false
        };
    }

    toggleSlideOutOptions() {
        this.setState({
            isOpen: this.state.slideOutOptions.length &&
                    !this.state.isOpen
        });
    }

    setSlideOutOptions(slideOutOptions) {
        this.setState({
            slideOutOptions: slideOutOptions
        });
    }

    renderButtons() {
        let renderedButtons = [];

        return (
            <div className="row">
                {renderedButtons}
            </div>
        );
    }

    renderSlideOutOptions() {
        let options = [];

        return (
            <div>
                {options}
            </div>
        )
    }

    render() {
        return (
            <bpcore.Card
                className="qdquarry__action-bar color-site-background-light"
                elevation={bpcore.Elevation.ONE}>

                <div className="qdquarry__action-bar-button-row">
                    {this.renderButtons()}
                    <bpcore.Icon className="qdquarry__download-button" icon="download" />
                    <bpcore.Icon className="qdquarry__edit-button" icon="edit" />
                    <bpcore.Icon className="qdquarry__move-button" icon="move" />
                    <bpcore.Icon className="qdquarry__delete-button" icon="delete" />
                    <bpcore.Icon className="qdquarry__view-button" icon="view" />
                    <bpcore.Icon className="qdquarry__duplicate-button" icon="duplicate" />
                    <bpcore.Icon className="qdquarry__add-button" icon="add-to-artifact" />
        
                </div>
                
                <bpcore.Collapse
                    className="qdquarry__action-bar-slideout">
                    <hr />
                    {this.renderSlideOutOptions()}
                </bpcore.Collapse>
            </bpcore.Card>
        );
    }
}