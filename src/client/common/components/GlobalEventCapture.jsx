/* jshint ignore:start */

import * as React from 'react';

export class GlobalEventCapture extends React.Component {
    render() {
        return (
            <div
                style={{
                    opacity: '0.01',
                    position: 'absolute',
                    width: '100vw',
                    height: '100vh',
                    top: '0',
                    left: '0'
                }}
                onMouseDown={this.props.onMouseDown}
                onMouseMove={this.props.onMouseMove}
                onMouseUp={this.props.onMouseUp}>
            &nbsp;
            </div>
        );
    }
}