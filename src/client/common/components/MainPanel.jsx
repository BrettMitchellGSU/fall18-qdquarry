/* jshint ignore:start */

import * as React from 'react';
import * as bpcore from '@blueprintjs/core';

import { connect } from 'react-redux';

class MainPanelPlaceholder extends React.PureComponent {
    render() {
        return (
            <div className="qdquarry__main-panel-placeholder-wrapper">
                <bpcore.Card
                    elevation={bpcore.Elevation.TWO}
                    className="qdquarry__main-panel-placeholder-card card-background">
                    
                    <bpcore.H4 className="qdquarry__main-panel-placeholder-text">Select or create an item to get started</bpcore.H4>
                    <bpcore.Icon className="qdquarry__main-panel-placeholder-arrow color-site-background-dark" iconSize={45} icon="arrow-left" />
                </bpcore.Card>
            </div>
        );
    }
}

export class mainPanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = props;
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            ...nextProps
        });
    }

    render() {
        const { currentTool, toolContext } = this.state;

        const Tool = this.state.getTool(currentTool) || MainPanelPlaceholder;

        return (
            <div className="qdquarry__main-panel color-site-background-dark">
                <Tool toolContext={toolContext} dispatch={this.state.dispatch} />
            </div>
        );
    }
}

function PropsFromStore(store) {
    return {
        currentTool: store.mainPanel.currentTool,
        toolContext: store.mainPanel.toolContext
    }
}

export const MainPanel = connect(PropsFromStore)(mainPanel);
