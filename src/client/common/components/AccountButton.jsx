/* jshint ignore:start */

import * as React from 'react';
import * as bpcore from '@blueprintjs/core';

export class AccountButton extends React.PureComponent {

    render() {
        return (
            <bpcore.Icon className="qdquarry__account-button" icon="user" />
        );
    }

}