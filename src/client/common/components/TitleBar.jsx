/* jshint ignore:start */

import * as React from 'react';
import * as bpcore from '@blueprintjs/core';

import { AccountButton } from './AccountButton';

export class TitleBar extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        let pageLabel = this.props.pageLabel ?
                        ' | ' + this.props.pageLabel :
                        '';
        
        return (
            <bpcore.Card
                id="title-bar"
                className="qdquarry__title-bar color-site-background-light"
                elevation={bpcore.Elevation.TWO}
                >
                    <bpcore.H3 className="qdquarry__title-bar-title">QDQuarry{pageLabel}</bpcore.H3>
                    <AccountButton />
            </bpcore.Card>
        );
    }

}