

/* jshint ignore:start */

// Copyright (c) 2017 PlanGrid, Inc.

import React from 'react';
import * as bpcore from '@blueprintjs/core';

const Loading = () => (
  <div className="qdquarry__tool-loading">
    <bpcore.Spinner size={150} />
  </div>
);

export default Loading;
