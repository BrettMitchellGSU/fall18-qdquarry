// Copyright (c) 2017 PlanGrid, Inc.

/* jshint ignore:start */

import React, { Component } from 'react';
import axios from 'axios';

import Error from '../error';
import Loading from '../loading';

function withFetching(WrappedComponent) {
  return class extends Component {
    constructor(props) {
      super(props);
      this.state = {
        axiosConfig: props.axiosConfig
      };
      this.axiosCancelSource = axios.CancelToken.source();
      
      axios({
        ...this.state.axiosConfig,
        cancelToken: this.axiosCancelSource.token
      })
      .then((res) => {
        if (res.status >= 400) {
          this.setState({ error: `fetch error with status ${res.status}`});
          return;
        }
        this.setState({ data: res.data });
      })
      .catch((e) => {
        if (axios.isCancel(e)) {
          if (this.props.onCancel)
            this.props.onCancel(e);
          this.setState({ error: 'fetch cancelled' });
        } else {
          this.props.onError(e);
          this.setState({ error: 'fetch error' });
        }
      });
    }

    componentWillReceiveProps(newProps) {
      if (JSON.stringify(this.state.axiosConfig) !== JSON.stringify(newProps.axiosConfig)) {
        this.setState({
          axiosConfig: newProps.axiosConfig,
          data: null,
          error: null
        });
      
        axios({
          ...this.state.axiosConfig,
          cancelToken: this.axiosCancelSource.token
        })
        .then((res) => {
          if (res.status >= 400) {
            this.setState({ error: `fetch error with status ${res.status}`});
            return;
          }
          this.setState({ data: res.data });
        })
        .catch((e) => {
          if (axios.isCancel(e)) {
            if (this.props.onCancel)
              this.props.onCancel(e);
            this.setState({ error: 'fetch cancelled' });
          } else {
            this.props.onError(e);
            this.setState({ error: 'fetch error' });
          }
        });
      }
    }

    componentWillUnmount() {
      this.abort();
    }

    render() {
      if (this.state.error) {
        return <Error {...this.props} error={this.state.error} />;
      }

      if (this.state.data) {
        return <WrappedComponent data={this.state.data} {...this.props} />;
      }
      return (
        <Loading />
      );
    }

    abort() {
      this.axiosCancelSource.cancel('Operation cancelled by unmounting');
    }
  };
}

export default withFetching;
