
/* jshint ignore:start */

import React, { Component } from 'react';

import * as bpcore from '@blueprintjs/core';

import { SelectableText } from './SelectableText';

import withFetching from './fetch-wrapper';

class txtViewer extends Component {
    constructor(props) {
        super(props);
        // Split the data in to individual lines and display in paragraph tags
        if (props.data && typeof props.data === 'string')
            this.state = {
                data: props.data,
                paragraphs: props.data
                                 .split(/(?:\r\n|\n)/)
                                 .map(p => (<p className="qdquarry__txt-viewer-paragraph">{p}</p>))
            };
        else
            this.state = {
                error: (<bpcore.Callout className="qdquarry__txt-viewer-error-callout">File not found</bpcore.Callout>)
            };
    }

    componentWillReceiveProps(newProps) {
        if (this.state.data !== newProps.data)
            this.setState({
                data: newProps.data,
                paragraphs: newProps.data
                                .split(/(?:\r\n|\n)/)
                                .map(p => (<p className="qdquarry__txt-viewer-paragraph">{p}</p>))
            });
    }

    onMouseDown(e) {
        
    }

    render() {
        return (
            <div className="qdquarry__txt-viewer-wrapper">
                <bpcore.Card className="qdquarry__txt-viewer-viewport">
                    <div 
                        className="qdquarry__txt-viewer-viewport-inner"
                        onMouseDown={this.onMouseDown}>
                        {this.state.paragraphs ? 
                        
                            <SelectableText dispatch={this.props.dispatch}
                                            addCode={this.props.addCode}>
                            {this.state.paragraphs}
                            </SelectableText> :

                            this.state.error}
                    </div>
                </bpcore.Card>
            </div>
        );
    }
}

const TxtViewer = withFetching(txtViewer);

export default TxtViewer;
