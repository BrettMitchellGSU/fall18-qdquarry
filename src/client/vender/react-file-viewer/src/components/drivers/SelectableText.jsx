
/* jshint ignore:start */

import * as React from 'react';

import * as bpcore from '@blueprintjs/core';


// Adapted from https://github.com/noemaireamiot/react-selected-text-menu/blob/master/src/SelectedMenu.js
export class SelectableText extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selection: ''
        }
    }

    clearSelection = () => {
        this.setState({ selection: '' });
    }

    onContextMenu = (e) => {
        this.setState({ selection: window.getSelection().toString() });

        const oldSelectionRange = window.getSelection().getRangeAt(0);

        e.preventDefault();
        e.stopPropagation();

        const ePos = {
            x: e.clientX,
            y: e.clientY
        }

        // Timeouts are necessary to allow changes to the DOM to propagate
        // before making changes to the current selection
        setTimeout(() => {
            bpcore.ContextMenu.show(
            (<bpcore.Menu>
                    <bpcore.MenuItem disabled={this.state.selection === ''} 
                                    icon='label'
                                    text='Add Code...'
                                    onClick={this.props.addCode}/>
                    <bpcore.MenuItem disabled={true} icon='label' text='Highlight...' />
                    <bpcore.MenuItem disabled={true} icon='annotation' text='Add Note...' />
                    <bpcore.MenuItem disabled={true} icon='bookmark' text='Add Bookmark...' />
                    <bpcore.MenuDivider />
                    <bpcore.MenuItem disabled={true} icon='label' text='Cut...' />
                    <bpcore.MenuItem disabled={true} icon='label' text='Copy...' />
                    <bpcore.MenuItem disabled={true} icon='label' text='Paste...' />
                    <bpcore.MenuItem disabled={true} icon='label' text='Delete...' />
                    <bpcore.MenuDivider />
                    <bpcore.MenuItem disabled={true} icon='search' text='Find in Workspace...' />
                    <bpcore.MenuItem disabled={true} icon='globe-network' text='Search the Internet...' />
                </bpcore.Menu>),
                {left: ePos.x, top: ePos.y},
                /* On close */
                () => {

                }
            )

            // Restore selection that was cleared by showing the context menu
            setTimeout(() => {
                window.getSelection().addRange(oldSelectionRange);
            }, 1);
        }, 1);
    }

    componentDidMount() {
        document.addEventListener('click', this.clearSelection);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.clearSelection);
    }

    selectionRangeContainsEventClick = (e, range) => {
        const boundingRect = range.getBoundingClientRect();
        return (boundingRect.x                       <= e.clientX &&
                boundingRect.x + boundingRect.width  >= e.clientX &&
                boundingRect.y                       <= e.clientY &&
                boundingRect.y + boundingRect.height >= e.clientY   );
    }

    onMouseDown = (e) => {
        const sel = window.getSelection();

        if (window.getSelection().toString() !== '') {
            for (let i = 0; i < sel.rangeCount; i++) {
                if (this.selectionRangeContainsEventClick(e, sel.getRangeAt(i)))
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
        }
    }

    getOverallSelectionIndex = (container, startIdx) => {
        const siblingArray = Array.from(container.parentNode.children);
        const containerIdx = siblingArray.indexOf(container);
        const preceedingLength = 
            siblingArray
            .map(s => (s.innerText || s.textContent).length )
            .reduce(
                (prev, cur, idx) => {
                        return idx < containerIdx ?
                            prev + cur :
                            prev;
                }, 0);
        return preceedingLength +
              (container.innerText || container.textContent).length - startIdx;
    }

    onMouseUp = (e) => {
        const selection = window.getSelection();
        if (selection.toString() !== '') {
            this.props.dispatch({
                type: 'SELECT_TEXT',
                toolType: 'FILE_CODE_TOOL',
                payload: {
                    selectionContents: selection.toString(),
                    selectionStart: this.getOverallSelectionIndex(
                        selection.anchorNode,
                        selection.anchorOffset
                    ),
                    selectionEnd: this.getOverallSelectionIndex(
                        selection.focusNode,
                        selection.focusOffset
                    )
                }
            });
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
    }

    render() {
        return (
            <div
                onContextMenu={this.onContextMenu}
                onMouseDown={this.onMouseDown}
                onMouseUp={this.onMouseUp}>
                
                {this.props.children}
            </div>
        );
    }

}
