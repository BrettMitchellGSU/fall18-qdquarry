
/* jshint ignore:start */

import { getCodeHierarchy, getFileHierarchy } from './ListActions';

export const InitializeAction = function() {
    return function(dispatch) {
        dispatch(getCodeHierarchy());
        dispatch(getFileHierarchy());
    };
}
