
/* jshint ignore:start */

import { getWorkspaceIDFromURLParameter } from '../../common/util/helpers';
import { Permute } from '../../common/util/PromiseActionPermutations';

import { API } from '../../common/util/API';

export const CodeActions = {
    ...Permute('FETCH_CODES', 'RENAME_CODE', 'MOVE_CODE', 'CREATE_CODE', 'DELETE_CODE'),
};

export const FileActions = {
    ...Permute('FETCH_FILES', 'RENAME_FILE', 'MOVE_FILE', 'CREATE_FILE', 'DELETE_FILE'),
};

export function getCodeHierarchy() {
    return {
        type: CodeActions.FETCH_CODES,
        payload: API('/a/hierarchy/get-hierarchy',
                    {scope: 'workspace',
                     scopeID: getWorkspaceIDFromURLParameter(),
                     type: 'code'})
    };
}

export function getFileHierarchy() {
    return {
        type: FileActions.FETCH_FILES,
        payload: API('/a/hierarchy/get-hierarchy',
                    {scope: 'workspace',
                     scopeID: getWorkspaceIDFromURLParameter(),
                     type: 'file'})
    };
}

/**
 * Action for moving an item in the ListView
 * @param {string} hierarchyType Determines which ListViewPane will be operated on
 * @param {string} itemType Specifies a folder or an item
 * @param {string} item Specifies the item to move
 * @param {string} from Old item location
 * @param {string} to New item location
 */
export function moveFileItem(itemType, itemName, from, to) {
    return {
        type: ListActionsCommon.RENAME,
        payload: API('/a/hierarchy/move-item',
                     {scope: 'workspace',
                      scopeID: getWorkspaceIDFromURLParameter(),
                      type: 'file',
                      oldItemPath: from,
                      newItemPath: to,
                      itemName: itemName})
    }
}

export const renameItem = (hierarchyType) => (parentDirectory, oldName, newName) => {
    return {
        type: (hierarchyType === 'file' ? FileActions.RENAME : CodeActions.RENAME),
        payload: API('/a/hierarchy/rename-item',
                     {scope: 'workspace',
                      scopeID: getWorkspaceIDFromURLParameter(),
                      type: 'file',
                      itemPath: parentDirectory,
                      oldName: oldName,
                      newName: newName})
    }
}

export function createItem(hierarchyType, itemType, containingFolder, folderIndex) {
    
}

export function finalizeCreateItem(hierarchyType, itemType, containingFolder, folderIndex, itemData) {
    
}

export function deleteItem(hierarchyType, itemType, item) {
    
}