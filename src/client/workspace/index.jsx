
/* jshint ignore:start */

// Libaries
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

// Components
import { ContentScaffold } from '../common/components/ContentScaffold';
import { ListViewButtonSet } from './components/list-view/ListViewButtonSet';
import { FileCodeTool } from './components/FileCodeTool';
import { CodeViewTool } from './components/CodeViewTool';

// Store
import { store } from './store';

// Actions
import { InitializeAction } from './actions/InitializeAction';

// Helpers
import { getURLParameter } from '../common/util/helpers';

const toolMap = {
    CODE_VIEW_TOOL: CodeViewTool,
    FILE_CODE_TOOL: FileCodeTool
};

ReactDOM.render((
    <Provider store={store}>
        <ContentScaffold listViewButtonSet={ListViewButtonSet}
                         mode='workspace'
                         for={getURLParameter(1)}
                         toolMap={toolMap}
                         pageLabel={'Workspace'} />
    </Provider>),
    document.getElementById('root')
);

// Initialize the UI
store.dispatch(InitializeAction());
