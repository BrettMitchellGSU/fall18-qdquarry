
import { applyMiddleware, createStore } from 'redux';

import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';

import { combinedReducers } from './reducers';

const middleware = applyMiddleware(promise(), thunk, createLogger());

export const store = createStore(combinedReducers, middleware);
