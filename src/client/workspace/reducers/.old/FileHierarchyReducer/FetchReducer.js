
/* jshint ignore:start */

import { FileActions } from '../../actions/ListActions';

import { randomHex } from '../../../../common/helpers';

const initialState = {
    fetching: false,
    hierarchy: null,
    error: null,
    dataIdentifier: randomHex(32)
};

export function FetchReducer(state = initialState, action) {

    switch (action.type) {
        case FileActions.FETCH_PENDING:
            return {...state, fetching: true};
        case FileActions.FETCH_REJECTED:
            return {...state, fetching: false, error: action.payload.error};
        case FileActions.FETCH_FULFILLED:
            return {
                ...state,
                fetching: false,
                hierarchy: action.payload.data.hierarchy,
                dataIdentifier: randomHex(32)
            };
    }
    return state;
}
