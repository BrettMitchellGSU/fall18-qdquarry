
/* jshint ignore:start */

import { FileActions } from '../../actions/ListActions';

import { randomHex } from '../../../../common/helpers';

const initialState = {
    fetching: false,
    hierarchy: null,
    error: null,
    dataIdentifier: randomHex(32)
};

export function RenameReducer(state = initialState, action) {

    switch (action.type) {
        case 'file' + ListActionsCommon.RENAME_PENDING:
            return {...state, fetching: true};
        case 'file' + ListActionsCommon.RENAME_REJECTED:
            return {...state, fetching: false, error: action.payload.error};
        case 'file' + ListActionsCommon.RENAME_FULFILLED:
            return {
                ...state,
                fetching: false,
                hierarchy: action.payload.data.hierarchy,
                dataIdentifier: randomHex(32)
            };
    }
    return state;
}
