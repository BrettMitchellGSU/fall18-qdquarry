
/* jshint ignore:start */

import { CodeActions } from '../../actions/ListActions';

import { randomHex } from '../../../../common/helpers';

const initialState = {
    fetching: false,
    hierarchy: null,
    error: null,
    dataIdentifier: randomHex(32)
};

export function FetchReducer(state = initialState, action) {

    switch (action.type) {
        case CodeActions.FETCH_PENDING:
            return {...state, fetching: true};
        case CodeActions.FETCH_REJECTED:
            return {...state, fetching: false, error: action.payload.error};
        case CodeActions.FETCH_FULFILLED:
            return {
                ...state,
                fetching: false,
                hierarchy: action.payload.data.hierarchy,
                dataIdentifier: randomHex(32)
            };
    }
    return state;
}
