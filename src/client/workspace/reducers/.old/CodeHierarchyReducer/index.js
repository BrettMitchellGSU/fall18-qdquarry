
/* jshint ignore:start */

import flatCombineReducers from 'flat-combine-reducers';

import { FetchReducer } from './FetchReducer';

export const CodeHierarchyReducer = flatCombineReducers(FetchReducer);
