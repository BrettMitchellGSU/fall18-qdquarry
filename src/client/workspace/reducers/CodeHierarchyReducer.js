
/* jshint ignore:start */

import { randomHex } from '../../../common/helpers';

const initialState = {
    fetching: false,
    hierarchy: null,
    error: null,
    dataIdentifier: randomHex(32)
};

export function CodeHierarchyReducer(state = initialState, action) {
    switch(action.type) {
    case 'FETCH_CODES_PENDING':
        return {
            ...state,
            fetching: true
        };

    case 'FETCH_CODES_REJECTED':
        return {
            ...state,
            fetching: false,
            error: action.payload
        };

    case 'FETCH_CODES_FULFILLED':
        return {
            ...state,
            fetching: false,
            hierarchy: action.payload.data.hierarchy,
            dataIdentifier: randomHex(32)
        };

    case 'RENAME_CODE_PENDING':
        return {
            ...state,
        };

    case 'RENAME_CODE_REJECTED':
        return {
            ...state,
        };

    case 'RENAME_CODE_FULFILLED':
        return {
            ...state,
        };

    case 'MOVE_CODE_PENDING':
        return {
            ...state,
        };

    case 'MOVE_CODE_REJECTED':
        return {
            ...state,
        };

    case 'MOVE_CODE_FULFILLED':
        return {
            ...state,
        };

    case 'DELETE_CODE_PENDING':
        return {
            ...state,
        };

    case 'DELETE_CODE_REJECTED':
        return {
            ...state,
        };

    case 'DELETE_CODE_FULFILLED':
        return {
            ...state,
        };

    case 'CREATE_CODE_PENDING':
        return {
            ...state,
        };

    case 'CREATE_CODE_REJECTED':
        return {
            ...state,
        };

    case 'CREATE_CODE_FULFILLED':
        return {
            ...state,
        };

    
    }

    return state;
}
