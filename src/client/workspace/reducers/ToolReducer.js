
/* jshint ignore:start */

const initialState = {
    currentTool: null,
    toolContext: {}
};

export function ToolReducer(state = initialState, action) {
    switch (action.toolType) {
        case 'FILE_CODE_TOOL':
            switch (action.type) {

            case 'ADD_SEGMENT_TO_CODE':
                return {
                    ...state,
                    toolContext: {
                        ...state.toolContext,
                        cursorVisible: false,
                        selectionContents: '',
                        addSegmentModalVisible: false
                    }
                }

            case 'SELECT_TEXT':
                return {
                    ...state,
                    toolContext: {
                        ...state.toolContext,
                        selectionStart: action.payload.selectionStart,
                        selectionEnd: action.payload.selectionEnd,
                        selectionContents: action.payload.selectionContents
                    }
                };

                // Starts a drag action by making the cursor display object
                // visible 
            case 'START_DRAG_TEXT':
                return {
                    ...state,
                    toolContext: {
                        ...state.toolContext,
                        cursorVisible: true,
                        cursorPosition: action.payload.cursorPosition,
                        selectionContents: action.payload.selectionContents
                    }
                };

            case 'END_DRAG_TEXT':
                return {
                    ...state,
                    toolContext: {
                        ...state.toolContext,
                        cursorVisible: false
                    }
                };
                
            case 'RIGHT_CLICK_SELECTED_TEXT':
                return {
                    ...state,
                    toolContext: {
                        ...state.toolContext,
                        contextMenuVisible: true,
                        contextMenuContents: action.payload.contextMenuContents,
                        contextMenuPosition: action.payload.contextMenuPosition
                    }
                };

            case 'CLICK_AWAY_SELECTED_TEXT':
                return {
                    ...state,
                    toolContext: {
                        ...state.toolContext,
                        contextMenuVisible: false
                    }
                };

            case 'CONTEXT_MENU_ADD_TO_CODE':
                return {
                    ...state,
                    toolContext: {
                        ...state.toolContext,
                        contextMenuVisible: false,
                        selectionVisible: false
                    }
                }
            
            case 'OPEN_ADD_SEGMENT_MODAL':
                return {
                    ...state,
                    toolContext: {
                        ...state.toolContext,
                        addSegmentModalVisible: true
                    }
                }
            
            case 'CLOSE_ADD_SEGMENT_MODAL':
                return {
                    ...state,
                    toolContext: {
                        ...state.toolContext,
                        addSegmentModalVisible: false
                    }
                }

                // Sets default toolContext for FileCodeTool
            case 'OPEN_TOOL':
                return {
                    ...state,
                    currentTool: 'FILE_CODE_TOOL',
                    toolContext: {
                        contextMenuVisible: false,
                        contextMenuContents: null,
                        contextMenuPosition: {top: 0, left: 0},

                        fileID: action.payload.fileID,
                        fileName: action.payload.fileName,

                        selectionStart: 0,
                        selectionEnd: 0,
                        selectionVisible: false,
                        selectionContents: '',

                        cursorDragPosition: {top: 0, left: 0},
                        cursorVisible: false,

                        addSegmentModalVisible: false
                    }
                }
            }
            break;

        case 'CODE_VIEW_TOOL':
            switch (action.type) {

            case 'MODIFY_SEGMENT':
                return {
                    ...state,
                    toolContext: {
                        ...toolContext,
                        segmentList: [
                            ...this.state.toolContext.segmentList.slice(0, actions.payload.modifyIdx - 1),
                            actions.payload.modifiedSegment,
                            ...this.state.toolContext.segmentList.slice(actions.payload.modifyIdx)
                        ]
                    }
                };

            case 'DELETE_SEGMENT':
                return {
                    ...state,
                    toolContext: {
                        ...toolContext,
                        segmentList: state
                                     .toolContext
                                     .segmentList
                                     .splice(action.payload.deleteIdx)
                    }
                };

            case 'OPEN_TOOL':
                return {
                    ...state,
                    currentTool: 'CODE_VIEW_TOOL',
                    toolContext: {
                        ...state.toolContext,
                        segmentList: [],
                        codeID: action.payload.codeID,
                        codeName: action.payload.codeName
                    }
                }
            }
        case 'ANY':
            switch (action.type) {
            case 'CLOSE_TOOL':
                return {
                    ...state,
                    currentTool: null,
                    toolContext: {}
                };
            }
    }

    return state;
}
