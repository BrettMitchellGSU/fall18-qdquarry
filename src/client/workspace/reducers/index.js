
import { combineReducers } from 'redux';

import { CodeHierarchyReducer } from './CodeHierarchyReducer';
import { FileHierarchyReducer } from './FileHierarchyReducer';

import { ToolReducer } from './ToolReducer';

export const combinedReducers = combineReducers({
    hierarchies: combineReducers({
        code: CodeHierarchyReducer,
        file: FileHierarchyReducer
    }),
    mainPanel: ToolReducer
});
