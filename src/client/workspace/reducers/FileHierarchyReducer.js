
/* jshint ignore:start */

import { randomHex } from '../../../common/helpers';

const initialState = {
    fetching: false,
    hierarchy: null,
    error: null,
    dataIdentifier: randomHex(32)
};

export function FileHierarchyReducer(state = initialState, action) {
    switch(action.type) {
    case 'FETCH_FILES_PENDING':
        return {
            ...state,
            fetching: true
        };

    case 'FETCH_FILES_REJECTED':
        return {
            ...state,
            fetching: false,
            error: action.payload
        };

    case 'FETCH_FILES_FULFILLED':
        return {
            ...state,
            fetching: false,
            hierarchy: action.payload.data.hierarchy,
            dataIdentifier: randomHex(32)
        };

    case 'RENAME_FILE_PENDING':
        return {
            ...state,
        };

    case 'RENAME_FILE_REJECTED':
        return {
            ...state,
        };

    case 'RENAME_FILE_FULFILLED':
        return {
            ...state,
        };

    case 'MOVE_FILE_PENDING':
        return {
            ...state,
        };

    case 'MOVE_FILE_REJECTED':
        return {
            ...state,
        };

    case 'MOVE_FILE_FULFILLED':
        return {
            ...state,
        };

    case 'DELETE_FILE_PENDING':
        return {
            ...state,
        };

    case 'DELETE_FILE_REJECTED':
        return {
            ...state,
        };

    case 'DELETE_FILE_FULFILLED':
        return {
            ...state,
        };

    case 'CREATE_FILE_PENDING':
        return {
            ...state,
        };

    case 'CREATE_FILE_REJECTED':
        return {
            ...state,
        };

    case 'CREATE_FILE_FULFILLED':
        return {
            ...state,
        };

    
    }

    return state;
}
