
/* jshint ignore:start */

import * as React from 'react';
import * as ReactDOM from 'react-dom';

import * as bpcore from '@blueprintjs/core';

const trackerOffset = {
    top: -5,
    left: -5
}

export class CursorTracker extends React.Component {
    render() {
        const cursorTop = ( this.props.toolContext.cursorPosition     && 
                            this.props.toolContext.cursorPosition.top ||
                            -trackerOffset.top ) + trackerOffset.top;
        const cursorLeft = ( this.props.toolContext.cursorPosition      && 
                             this.props.toolContext.cursorPosition.left ||
                             -trackerOffset.left ) + trackerOffset.left;
        return (
            <div 
                className="qdquarry__cursor-tracker-wrapper"
                style={{visibility: (this.props.toolContext.cursorVisible ? 'visible' : 'hidden'),
                        top: cursorTop, left: cursorLeft }}>
                <bpcore.Tag
                    className="qdquarry__cursor-tracker"
                    multiline={false}
                    intent={bpcore.Intent.PRIMARY}
                    round={true}
                    icon='label'>
                    {this.props.toolContext.cursorContents}
                </bpcore.Tag>
            </div>
        );
    }
}
