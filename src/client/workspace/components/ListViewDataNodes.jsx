import { ListViewActionAndRename } from "./list-view/ListViewActionAndRename";

/* jshint ignore:start */

/**
 * ListViewNode is the base node type for the ListView
 */
class ListViewNode {
    constructor({
        icon = 'circle',
        itemName = ''
    }) {
        this.icon = icon;
        this.itemName = itemName;
    }

    makeTreeNode = (prevId) => { }
}

/**
 * ListViewItemNode represents an item. This could be a workspace, file, or
 * other.
 */
export class ListViewItemNode extends ListViewNode {
    constructor({
        awsItem = null,
        icon = 'document',
        itemName = '',
        actionButtonGenerator = null,
        targetContext = {}
    }) {
        super({
            icon: icon,
            itemName: itemName
        });

        this.targetContext = targetContext;

        if (awsItem)
            this.fromAWSItem(awsItem);
        
        this.actionButtons = actionButtonGenerator(this);
    }

    fromAWSItem(item) {
        this.itemName = item.name;
        this.targetContext = {
            ID: item.value[Object.keys(item.value)[0]],
            name: item.name,
            hierarchyType: item.hierarchyType,
            parentDirectory: item.parentDirectory
        };
    }

    makeTreeNode = (prevId) => {
        return {
            id: ++prevId,
            hasCaret: false,
            icon: this.icon,
            label: this.itemName,
            secondaryLabel: <div>{this.actionButtons}</div>
        };
    }
}

/**
 * ListViewDirectoryNode represents a directory. It is the only type of list
 * view node that can hav children.
 */
export class ListViewDirectoryNode extends ListViewNode {
    constructor({
        awsFolder = null,
        itemName = '',
        itemActionButtonGenerator = () => null,
        folderActionButtonGenerator = () => null,
        childNodes = []
    }) {
        super({
            icon: 'folder-close',
            itemName: itemName
        });

        this.childNodes = childNodes;

        this.folderActionButtonGenerator = folderActionButtonGenerator;
        this.itemActionButtonGenerator = itemActionButtonGenerator;

        if (awsFolder)
            this.fromAWSFolder(awsFolder);
        
        this.actionButtons = folderActionButtonGenerator(this);
    }

    fromAWSFolder(folder) {
        if (!('M' in folder.value)) return;

        this.itemName = folder.name;

        this.targetContext = {
            name: folder.name,
            hierarchyType: folder.hierarchyType,
            parentDirectory: folder.parentDirectory
        };

        this.childNodes = [];

        const folderContents = folder.value.M;
        for (const k of Object.keys(folderContents))
            this.childNodes.push(
                ('M' in folderContents[k] ?
                    new ListViewDirectoryNode({
                        awsFolder: {
                            parentDirectory: folder.parentDirectory + '/' + folder.name,
                            hierarchyType: folder.hierarchyType,
                            name: k, 
                            value: folderContents[k]},
                        folderActionButtonGenerator: this.folderActionButtonGenerator,
                        itemActionButtonGenerator: this.itemActionButtonGenerator
                    }) :
                    new ListViewItemNode({
                        awsItem: {
                            parentDirectory: folder.parentDirectory + '/' + folder.name,
                            hierarchyType: folder.hierarchyType,
                            name: k,
                            value: folderContents[k]},
                        actionButtonGenerator: this.itemActionButtonGenerator})));
    }

    makeTreeNode = (prevId) => {
        let treeChildNodes = [];

        let _prevId = prevId++;

        for (let child of this.childNodes)
            treeChildNodes.push(
                child.makeTreeNode(++prevId)
            );

        return {
            id: _prevId + 1,
            hasCaret: true,
            icon: this.icon,
            label: this.itemName,
            secondaryLabel: <div>{this.actionButtons}</div>,

            childNodes: treeChildNodes
        };
    }
}