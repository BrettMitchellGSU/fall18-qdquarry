
/* jshint ignore:start */

import * as React from 'react';
import { connect } from 'react-redux';

import * as bpcore from '@blueprintjs/core';

class toolHeader extends React.Component {
    render() {
        return (
            <bpcore.Card
                className="qdquarry__file-viewer-header-card"
                elevation={bpcore.Elevation.TWO}>
                <div className="qdquarry__file-viewer-header-row">
                    <bpcore.Icon icon="document" className="qdquarry__file-viewer-header-icon" iconSize={25} />
                    <bpcore.H6 className="qdquarry__file-viewer-header-title">{this.props.title}</bpcore.H6>
                    <bpcore.NavbarDivider className="qdquarry__file-viewer-header-item" />
                    <bpcore.Button
                        className="qdquarry__file-viewer-header-item qdquarry__file-viewer-header-close-button"
                        onClick={() => { this.props.dispatch({type: 'CLOSE_TOOL', toolType: 'ANY'}); }}>
                        <bpcore.Icon icon="cross"/>
                    </bpcore.Button>
                </div>
            </bpcore.Card>
        )
    }
}

export const ToolHeader = connect()(toolHeader);
