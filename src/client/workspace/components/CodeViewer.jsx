
/* jshint ignore:start */

import * as React from 'react';

import * as bpcore from '@blueprintjs/core';

import withFetching from '../../vender/react-file-viewer/src/components/drivers/fetch-wrapper';

class codeViewer extends React.Component {

    constructor(props) {
        super(props);

        if (props.data) {
            this.state = {
                data: props.data.code
            }
        } else {
            this.state = {
                error: 'Code not found'
            }
        }
    }

    renderSegmentCard = (segment) => {
        console.log(segment);
        segment = segment.M;
        return (
            <bpcore.Card className="qdquarry__code-viewer-card">
                <bpcore.H5 className="qdquarry__code-viewer-card-title">
                    {segment.filename.S}
                </bpcore.H5>
                <bpcore.Callout className="qdquarry__code-viewer-card-contents">
                    {segment.segment_contents.S}
                </bpcore.Callout>
            </bpcore.Card>
        );
    }

    render() {
        console.log(this.state);

        return (
            <div className="qdquarry__code-viewer-wrapper">
                <bpcore.Card className="qdquarry__code-viewer-viewport">
                    <div className="qdquarry__code-viewer-viewport-inner">
                        {
                            (
                                this.state.data ? 
                                this.state.data.segments.map(this.renderSegmentCard) :
                                <div>{this.state.error}</div>
                            )
                        }
                    </div>
                </bpcore.Card>
            </div>
        );
    }
}

export const CodeViewer = withFetching(codeViewer);
