
/* jshint ignore:start */

import * as React from 'react';

import { connect } from 'react-redux';
import { ToolHeader } from './ToolHeader';
import { CodeViewer } from './CodeViewer';

import { getWorkspaceIDFromURLParameter } from '../../common/util/helpers';

export class codeViewTool extends React.Component {
    render() {
        return (
            <div className="code-view-tool-wrapper">
                <div className="qdquarry__code-view-tool-code-viewer">
                    <ToolHeader title={this.props.toolContext.codeName} />

                    <CodeViewer 
                        dispatch={this.props.dispatch}
                        axiosConfig={{
                            url:'/a/code/get-code',
                            method: 'POST',
                            data: {
                                codeID: this.props.toolContext.codeID,
                                workspaceID: getWorkspaceIDFromURLParameter()
                            }
                        }} />
                </div>
            </div>
        )
    }
}

function PropsFromStore(store) {
    return {
        toolContext: store.mainPanel.toolContext
    };
}

export const CodeViewTool = connect(PropsFromStore)(codeViewTool);
