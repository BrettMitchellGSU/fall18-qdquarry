
/* jshint ignore:start */

import * as React from 'react';
import * as bpcore from '@blueprintjs/core';

import { GlobalEventCapture } from '../../../common/components/GlobalEventCapture';

export class WidthControl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dragging: false,
            lastDragX: 0
        }
    }

    onMouseDown = (e) => {
        window.addEventListener('mouseup', this.onMouseUp, true);
        window.addEventListener('mousemove', this.onMouseMove, true);

        this.setState({
            dragging: true,
            lastDragX: e.clientX
        });
    }

    onMouseMove = (e) => {
        if (this.state.dragging) {
            e.preventDefault();
            this.props.modifyWidth(e.clientX - this.state.lastDragX);
            this.setState({
                lastDragX: e.clientX
            });
        }
    }

    onMouseUp = (e) => {
        e.preventDefault();
        this.setState({
            dragging: false
        });
    }

    render() {
        let globalInput = this.state.dragging ? [(<GlobalEventCapture key={0} />)] : [];

        return (
            <div 
                className="qdquarry__list-view-width-control color-width-control"
                onMouseDown={this.onMouseDown}>
                <bpcore.Icon
                    className="qdquarry__list-view-width-control-icon"
                    icon="double-caret-horizontal"
                    iconSize={8}></bpcore.Icon>
                {globalInput}
            </div>
        );
    }
}