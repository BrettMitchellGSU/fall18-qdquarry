
/* jshint ignore:start */

import { ListViewActionButton } from "./ListViewActionButton";
import { ListViewNodeControl } from "./ListViewNodeControl";

/**
 * ListViewButtonSets defines the buttons available to the user in the ListView.
 * Each entry is a method that returns a list of ListViewActionButton's. When a
 * ListViewNode gets its respective button set, it passes in its state
 */
export const ListViewButtonSet = {
    file: {
        folder: (f) => {
                return <ListViewNodeControl
                            contextTarget={f.contextTarget}
                            renameButton={
                                <ListViewActionButton
                                    icon={'text-highlight'}
                                    clickAction={() => {console.log('renaming code folder ' + f.contextTarget)}} />
                            }
                            buttons={[
                                <ListViewActionButton
                                    icon={'folder-new'}
                                    clickAction={() => {console.log('adding file folder to existing folder ' + f.contextTarget)}} />
                            ]} />;
            },
        item: (i) => {
            return <ListViewNodeControl
                        contextTarget={i.contextTarget}
                        renameButton={
                            <ListViewActionButton
                                icon={'text-highlight'}
                                clickAction={() => {console.log('renaming code ' + i.contextTarget)}} />
                        }
                        buttons={[
                            <ListViewActionButton
                                icon={'document-open'}
                                clickAction={ (dispatch) => {
                                    dispatch({
                                        type: 'CLOSE_TOOL',
                                        toolType: 'ANY'
                                    })
                                    dispatch({
                                        type: 'OPEN_TOOL',
                                        toolType: 'FILE_CODE_TOOL',
                                        payload: {
                                            fileID: i.contextTarget.ID,
                                            fileName: i.contextTarget.name }});
                                } } />
                        ]} />
        }
    },
    
    code: {
        folder: (f) => {
            return <ListViewNodeControl
                        contextTarget={f.contextTarget}
                        renameButton={
                            <ListViewActionButton
                                icon={'text-highlight'}
                                clickAction={() => {console.log('renaming code ' + i.contextTarget)}} />
                        }
                        buttons={[
                            <ListViewActionButton
                                icon={'folder-new'}
                                clickAction={() => {console.log('renaming code folder ' + f.contextTarget)}} />
                        ]} />
        },
        item: (i) => {
            return <ListViewNodeControl
                        contextTarget={i.contextTarget}
                        renameButton={
                            <ListViewActionButton
                                icon={'text-highlight'}
                                clickAction={() => {console.log('renaming code ' + i.contextTarget)}} />
                        }
                        buttons={[
                            <ListViewActionButton
                                icon={'tint'}
                                clickAction={() => {console.log('recoloring code ' + i.contextTarget)}} />,
                    
                            <ListViewActionButton
                                icon={'document-open'}
                                clickAction={(dispatch) => {
                                    dispatch({
                                        type: 'CLOSE_TOOL',
                                        toolType: 'ANY'
                                    })
                                    dispatch({
                                        type: 'OPEN_TOOL',
                                        toolType: 'CODE_VIEW_TOOL',
                                        payload: {
                                            codeID: i.contextTarget.ID,
                                            codeName: i.contextTarget.name
                                        }
                                    })
                                }} />
                        ]} />
        }
        
    }
};
