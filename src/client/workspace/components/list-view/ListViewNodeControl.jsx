
/* jshint ignore:start */

import * as React from 'react';
import * as bpcore from '@blueprintjs/core';
import { connect } from 'react-redux';

import { getWorkspaceIDFromURLParameter } from '../../../common/util/helpers';

import { renameItem } from '../../actions/ListActions';
import { API } from '../../../common/util/API';

class listViewNodeControl extends React.Component {

    constructor(props) {
        super(props);
        this.inputRef = React.createRef();
        this.state = {
            renaming: false
        };
        
        props.renameButton.props.clickAction = this.openRename;
    }

    componentDidUpdate() {
        if (this.state.renaming) {
            this.inputRef.current.focus();
        }
    }

    closeRename = (setNewName) => {
        if (setNewName) {
            const newName = this.inputRef.current.value;
            const {
                hierarchyType,
                name,
                parentDirectory
            } = this.props.contextTarget;
            
            this.props.dispatch(renameItem(hierarchyType)({
                parentDirectory: parentDirectory,
                oldName: name,
                newName: newName
            }));
        }
        
        document.removeEventListener('keydown', this.handleKey, false);

        this.inputRef.current.value = '';

        this.setState({
            renaming: false
        });
    }

    handleKey = (e) => {
        // ESC key code is 27
        if (e.keyCode === 27)
            this.closeRename(false);
        // Enter key code is 13
        else if (e.keyCode === 13)
            this.closeRename(true);
    }

    openRename = () => {
        document.addEventListener('keydown', this.handleKey, false);

        this.setState({
            renaming: true
        });
    }

    handleOnMouseUp = (e) => {
        if (this.props.toolContext.cursorVisible && 
            this.props.toolContext.cursorContents  ) {
            this.props.dispatch({
                type: 'ADD_SEGMENT_TO_CODE'
            });
            API('/a/code/add-segment', {
                codeID: this.contextTarget.ID,
                workspaceID: getWorkspaceIDFromURLParameter(),
                fileID: this.props.toolContext.fileID,
                fileName: this.props.toolContext.fileName,
                contents: this.props.toolContext.cursorContents,
                offset: 0
            });
        }
    }

    render() {
        // Blueprint pads the left of tree nodes with 23 pixels per level.
        // Shifting over by that amount per node prevents covering the drop-down
        // caret button on directories
        let lvl = 0;
        if (this.props.contextTarget)
            lvl = this.props
                    .contextTarget
                    .parentDirectory
                    .split('/')
                    .filter(d => d.length)
                    .length;

        const leftPaddingForTreeLevel = {
            marginLeft: 23 * lvl + 'px',
            width: 'calc(100% - ' + 23 * lvl + 'px)'
        }

        return (
            <div 
                onMouseUp={this.handleOnMouseUp}
                className="qdquarry__list-view-node-wrapper"
                style={leftPaddingForTreeLevel}>
                <bpcore.InputGroup
                    className="qdquarry__list-view-rename-input"
                    disabled={!this.state.renaming}
                    style={ (this.state.renaming ? {width: '100%'} : {display: 'none', width: '0px'}) }
                    inputRef={this.inputRef}
                    placeholder="Rename"
                    onBlur={() => this.closeRename(false)} />

                <div className="qdquarry__list-view-actions-wrapper"
                     style={ (this.state.renaming ? {display: 'none'} : {})}>
                    {this.props.renameButton}
                    {this.props.buttons}
                </div>
            </div>
        )
    }
}

function PropsFromStore(store) {
    return {
        toolContext: store.mainPanel.toolContext
    };
}

export const ListViewNodeControl = connect(PropsFromStore)(listViewNodeControl);
