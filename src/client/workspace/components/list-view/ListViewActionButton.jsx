
/* jshint ignore:start */

import * as bpcore from '@blueprintjs/core';
import { connect } from 'react-redux';

/**
 * ListViewActionButtonSet contains a row of buttons for item-specific actions.
 * This element is only shown when the user hovers over a tree item
 */
class listViewActionButton extends React.Component {
    constructor(props) {
        super(props);
        this.clickAction = props.clickAction || (() => {});
    }

    setClickAction = (action) => {
        this.clickAction = action;
    }

    render() {
        return (
            <bpcore.Button
                className="qdquarry__list-view-button"
                minimal={true}
                icon={this.props.icon}
                onClick={ () => { this.clickAction(this.props.dispatch); } }
                >
            </bpcore.Button>
        );
    }
}

export const ListViewActionButton = connect()(listViewActionButton);
