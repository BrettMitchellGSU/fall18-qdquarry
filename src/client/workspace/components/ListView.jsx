/* jshint ignore:start */

import * as React from 'react';
import { connect } from 'react-redux';
import * as bpcore from '@blueprintjs/core';

import { ListViewItemNode, ListViewDirectoryNode } from './list-view/ListViewDataNodes';
import { WidthControl } from './list-view/WidthControl';

class ListViewCollapsablePane extends React.Component {

    constructor(props) {
        super(props);
        this.childNodes = props.childNodes;
        this.state = {
            isOpen: !!props.isOpen
        };
    }

    componentWillReceiveProps(nextProps) {
        // Only call setState if the data has changed. Otherwise, re-renders
        // will propagate down and reload the main panel
        if (nextProps.dataIdentifier !== this.props.dataIdentifier)
            this.setState({
                data: this.makeTreeNodes(nextProps.childNodes)
            });
    }

    makeTreeNodes = (childNodes) => {
        let i = -1;
        return childNodes.map(
            v => v.makeTreeNode(++i)
        );
    }

    setTree = (nodes) => {
        this.childNodes = nodes;
        this.setState({
            data: this.makeTreeNodes()
        });
    }

    getNode(nodePath) {
        return ;
    }

    moveNode = (oldPath, newPath) => {
        let oldData = this.getNode(oldPath);
        this.removeNode(oldPath);
        this.addNode(oldData, newPath);
        this.setState({
            data: this.makeTreeNodes()
        });
    }

    removeNode(nodePath) { }
    addNode(nodeData, nodePath) { }
    renameNode(nodePath, newName) { }
    searchNodes(searchTerm) { }
    handleNodeClick(nodeData, nodePath, mouseEvent) {
        
    }

    handleNodeCollapse = (nodeData) => {
        nodeData.isExpanded = false;
        this.setState(this.state);
    }

    handleNodeExpand = (nodeData) => {
        nodeData.isExpanded = true;
        this.setState(this.state);
    }

    toggleOpen = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <bpcore.Card
                className="qdquarry__list-view-pane color-site-background-medium">

                <bpcore.Button
                    className="qdquarry__list-view-pane-title qdquarry__list-view-button color-site-background-light"
                    alignText="left"
                    onClick={this.toggleOpen}
                    icon={this.state.isOpen ? 'caret-down' : 'caret-right'}>
                
                {this.props.paneName}
                </bpcore.Button>

                <bpcore.Collapse
                    isOpen={this.state.isOpen}>

                    <bpcore.Tree
                        contents={this.state.data}
                        onNodeClick={this.handleNodeClick}
                        onNodeCollapse={this.handleNodeCollapse}
                        onNodeExpand={this.handleNodeExpand}>

                    </bpcore.Tree>
                </bpcore.Collapse>
            </bpcore.Card>
        );
    }
}

function getDataFromHierarchies(hierarchies, buttonSet) {
    let panes = [];
    for (const hierarchy in hierarchies) {
        let paneData = [];
        if (hierarchies[hierarchy].hierarchy) {
            const h = hierarchies[hierarchy].hierarchy;
            for (const item of Object.keys(h)) {
                if ('M' in h[item])
                    paneData.push(
                        new ListViewDirectoryNode({
                            awsFolder: {
                                parentDirectory: '/',
                                hierarchyType: hierarchy,
                                name: item,
                                value: h[item]},
                            folderActionButtonGenerator: buttonSet[hierarchy].folder,
                            itemActionButtonGenerator: buttonSet[hierarchy].item
                        }));
                else
                    paneData.push(
                        new ListViewItemNode({
                            awsItem: {
                                parentDirectory: '/',
                                hierarchyType: hierarchy,
                                name: item,
                                value: h[item]},
                            actionButtonGenerator: buttonSet[hierarchy].item}));
            }
        }
        panes.push({
            name: hierarchy,
            data: paneData
        });
    }

    return panes;
}

class listView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            width: props.width,
            data: props.placeholderData
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.hierarchies) {
            // Reduce dataIdentifiers to a single object with one concatenated
            // string for the new set of hierarchies and another set for the
            // current hierarchies
            const combinedIdentifiers = Object.keys(nextProps.hierarchies).reduce(
                (prev, cur) => {
                    return {newDID: prev.newDID + nextProps.hierarchies[cur].dataIdentifier,
                            curDID: prev.curDID + this.props.hierarchies[cur].dataIdentifier};
                }, {newDID: '', curDID: ''});
            if (combinedIdentifiers.newDID !== combinedIdentifiers.curDID)
                this.setState({
                    data: getDataFromHierarchies(nextProps.hierarchies, this.props.listViewButtonSet),
                    combinedDataIdentifier: combinedIdentifiers.newDID
                });
        }
    }

    modifyWidth = (pxDiff) => {
        let newWidth = this.state.width + pxDiff;
        newWidth = newWidth > this.props.minWidth ? newWidth : this.props.minWidth
        this.setState({width: newWidth});
        this.props.setOwnWidth(newWidth);
    }

    render() {
        let panes = [];

        let i = 0;
        for (let pane of this.state.data) {
            panes.push(
                <ListViewCollapsablePane
                    key={i++}
                    isOpen={true}
                    paneName={pane.name}
                    childNodes={pane.data}
                    dataIdentifier={this.state.combinedDataIdentifier} />);
        }

        return (
            <div
                className="qdquarry__list-view-wrapper color-site-background-medium">

                <bpcore.Card
                    className="qdquarry__list-view color-site-background-medium">
                    {panes}
                </bpcore.Card>
                <WidthControl
                    modifyWidth={this.modifyWidth}>

                </WidthControl>
            </div>
        );
    }
}

export const ListView = connect((store) => {
    return {
        hierarchies: store.hierarchies
    }
})(listView);
