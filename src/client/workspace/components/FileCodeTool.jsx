
/* jshint ignore:start */

import * as React from 'react';
import * as bpcore from '@blueprintjs/core';
import { connect } from 'react-redux';

import { CursorTracker } from './CursorTracker';
import { ToolHeader } from './ToolHeader';

import { TxtViewer } from '../../vender/react-file-viewer/src/components/drivers';
import { API } from '../../common/util/API';

import { getWorkspaceIDFromURLParameter } from '../../common/util/helpers';

export class fileCodeTool extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            toolContext: props.toolContext,
            displayingCursor: false,
            addingCode: false
        };

        this.cursorTag = <CursorTracker toolContext={this.state.toolContext} />

        this.segmentPathRef = React.createRef();
    }

    contextMenuAddCodeCallback = () => {
        this.props.dispatch({
            type: 'OPEN_ADD_SEGMENT_MODAL',
            toolType: 'FILE_CODE_TOOL'
        });
    }

    findCodeIDByPath = (path) => {
        const pathSplit = path.split('/')
                              .map(p => p.trim())
                              .filter(p => p.length);

        let current = this.props.codeHierarchy;
        while(pathSplit.length) {
            const p = pathSplit.shift();
            current = current[p];
            if (!current) return null;
            if (current.S) return current.S;
            if (current.M) current = current.M;
            else break;
        }

        return null;
    }

    addCodeSegment = () => {
        this.props.dispatch({
            type: 'ADD_SEGMENT_TO_CODE',
            toolType: 'FILE_CODE_TOOL'
        });

        const text = this.props.toolContext.selectionContents;
        const codeID = this.findCodeIDByPath(this.segmentPathRef.current.value);

        console.log(codeID);

        API(
            '/a/code/add-segment',
            {
                codeID: codeID,
                workspaceID: getWorkspaceIDFromURLParameter(),
                fileID: this.props.toolContext.fileID,
                fileName: this.props.toolContext.fileName,
                contents: text,
                offset: this.props.toolContext.selectionStart
            }
        ).then(d => console.log(d)).catch(e => console.log(e));
    }

    handleSegmentModalClose = () => {
        this.props.dispatch({
            type: 'CLOSE_ADD_SEGMENT_MODAL',
            toolType: 'FILE_CODE_TOOL'
        });
    }

    render() {
        return (
            <div className="qdquarry__file-code-tool-wrapper">
                <div className="qdquarry__file-code-tool-file-viewer">

                    <bpcore.Overlay isOpen={this.props.toolContext.addSegmentModalVisible}
                                    autoFocus={true}
                                    canEscapeKeyClose={true}
                                    onClose={this.handleSegmentModalClose}
                                    >

                        <div className="qdquarry__file-code-tool-segment-modal-wrapper">
                            <bpcore.Card className="qdquarry__file-code-tool-segment-modal">
                                <bpcore.H4>Add Segment to a code</bpcore.H4>
                                <bpcore.Callout
                                    icon='label'
                                    className="qdquarry__file-code-tool-segment-modal-quote">
                                
                                {this.props.toolContext.selectionContents}
                                </bpcore.Callout>

                                <bpcore.H6>Enter the code's path:</bpcore.H6>
                                <bpcore.InputGroup inputRef={this.segmentPathRef}></bpcore.InputGroup>
                                <div
                                    className="qdquarry__file-code-tool-segment-modal-submit">
                                    <bpcore.Button
                                        onClick={this.addCodeSegment}
                                        intent={bpcore.Intent.PRIMARY}>

                                        Submit
                                    </bpcore.Button>
                                </div>
                            </bpcore.Card>
                        </div>
                    </bpcore.Overlay>

                    <ToolHeader title={this.props.toolContext.fileName} />
                    <TxtViewer
                        dispatch={this.props.dispatch}
                        addCode={this.contextMenuAddCodeCallback}
                        onError={(e) => {console.log(e);}}
                        axiosConfig={{
                            url: '/a/file/download',
                            method: 'post',
                            data: {
                                fileID: this.props.toolContext.fileID,
                                userID: 'user1@demo.com'
                            }
                        }}
                        fileName={this.props.toolContext.fileName}
                        fileType={'txt'} />
                </div>
            </div>
        );
    }
}

function PropsFromStore(store) {
    return {
        toolContext: store.mainPanel.toolContext,
        codeHierarchy: store.hierarchies.code.hierarchy
    };
}

export const FileCodeTool = connect(PropsFromStore)(fileCodeTool);
