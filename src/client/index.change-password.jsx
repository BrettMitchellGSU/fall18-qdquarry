// jshint ignore: start

import * as React from 'react';
import ReactDOM from 'react-dom';

import { ChangePassword } from './js/change-password/ChangePassword';

ReactDOM.render(<ChangePassword/>, document.getElementById('root'));