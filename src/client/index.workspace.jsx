// jshint ignore: start

import * as React from 'react';
import ReactDOM from 'react-dom';

import { ContentScaffold } from './js/common/ContentScaffold';

ReactDOM.render(<ContentScaffold/>, document.getElementById('root'));