// jshint ignore: start

import * as React from 'react';
import ReactDOM from 'react-dom';

import { Homepage } from './js/homepage/Homepage';

ReactDOM.render(<Homepage/>, document.getElementById('root'));