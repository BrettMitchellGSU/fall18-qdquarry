
/* jshint ignore:start */

import * as React from 'react';
import * as bpcore from '@blueprintjs/core';

import { config } from '../common/CognitoClientConfig';
import { CognitoUserPool, AuthenticationDetails, CognitoUser } from 'amazon-cognito-identity-js';

export class ChangePassword extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            oldPassword: '',
            oldPasswordConfirm: '',
            newPassword: '',
            oldPasswordIntent: bpcore.Intent.NONE,
            changing: false
        };
    }

    changePassword = () => {
        this.setState({
            changing: true
        });

        let userPool = new CognitoUserPool(config.poolData);

        let user = new CognitoUser({
            Username: this.state.username,
            Pool: userPool
        });

        let authDetails = new AuthenticationDetails({
            Username: this.state.username,
            Password: this.state.oldPassword
        });

        user.authenticateUser(authDetails, {
            onSuccess: (res) => {
                user.changePassword(this.state.oldPassword, this.state.newPassword,
                    (err, res) => {
                        console.log(err, res);
        
                        this.setState({
                            changing: false
                        });
                    }
                );
            },
            onFailure: (err) => {
                this.setState({
                    changing: false
                });
            },
            newPasswordRequired: (userAttributes, requiredAttributes) => {
                user.changePassword(this.state.oldPassword, this.state.newPassword,
                    (err, res) => {
                        this.setState({
                            changing: false
                        });
                    }
                );
            }
        });
    }

    setUsername = (event) => {
        this.setState({
            username: event.target.value
        });
    }

    setOldPassword = (event) => {
        let intent = bpcore.Intent.DANGER;
        if (this.state.oldPasswordConfirm === event.target.value) {
            if (event.target.value.length > 0)
                intent = bpcore.Intent.SUCCESS;
            else
                intent = bpcore.Intent.NONE;
        }
        this.setState({
            oldPassword: event.target.value,
            oldPasswordsValid: intent
        });
    }

    setOldPasswordConfirm = (event) => {
        let intent = bpcore.Intent.DANGER;
        if (this.state.oldPassword === event.target.value) {
            if (event.target.value.length > 0)
                intent = bpcore.Intent.SUCCESS;
            else
                intent = bpcore.Intent.NONE;
        }
        this.setState({
            oldPasswordConfirm: event.target.value,
            oldPasswordIntent: intent
        });
    }

    setNewPassword = (event) => {
        this.setState({
            newPassword: event.target.value
        });
    }

    render() {
        let buttonContents = [];
        if (this.state.changing) {
            buttonContents.push(
                <bpcore.Spinner
                    className="qdquarry__login-spinner"
                    size={bpcore.Spinner.SIZE_SMALL}
                    intent={bpcore.Intent.PRIMARY} />
            )
        } else {
            buttonContents.push('Submit');
        }

        return (
            <div className="qdquarry__login-wrapper color-site-background-dark">
                <bpcore.Card
                    className="qdquarry__login-main-card color-site-background-light"
                    elevation={bpcore.Elevation.TWO}>

                    <bpcore.H4>Change your password</bpcore.H4>

                    <bpcore.InputGroup
                        className="qdquarry__login-field"
                        placeholder="Username"
                        disabled={this.state.changing}
                        onBlur={this.setUsername}
                        >

                    </bpcore.InputGroup>

                    <bpcore.InputGroup
                        className="qdquarry__login-field"
                        placeholder="Old Password"
                        disabled={this.state.changing}
                        onBlur={this.setOldPassword}
                        type="password"
                        intent={this.state.oldPasswordIntent}
                        >

                    </bpcore.InputGroup>

                    <bpcore.InputGroup
                        className="qdquarry__login-field"
                        placeholder="Confirm Old Password"
                        disabled={this.state.changing}
                        onBlur={this.setOldPasswordConfirm}
                        type="password"
                        intent={this.state.oldPasswordIntent}
                        >

                    </bpcore.InputGroup>

                    <bpcore.InputGroup
                        className="qdquarry__login-field"
                        placeholder="New Password"
                        disabled={this.state.changing}
                        onBlur={this.setNewPassword}
                        type="password">

                    </bpcore.InputGroup>

                    <bpcore.Button
                        className="qdquarry__login-submit-button"
                        disabled={this.state.changing}
                        onClick={this.changePassword}>

                        {buttonContents}
                    </bpcore.Button>
                </bpcore.Card>
            </div>
        );
    }
}