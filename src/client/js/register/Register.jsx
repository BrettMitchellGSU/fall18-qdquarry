/* jshint ignore:start */

import * as React from 'react';
import * as bpcore from '@blueprintjs/core';

import { config } from '../common/CognitoClientConfig';
import { CognitoUserPool, CognitoUserAttribute } from 'amazon-cognito-identity-js';

const errorMessages = {
    'UsernameExistsException': 'That username already exists',
    'InvalidParameterException': 'Please check your information. Passwords must be at least 8 characters long and must contain at least 1 uppercase letter, 1 lowercase letter, and one number. Make sure that the email you entered is properly formatted.'
}

export class Register extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            password: '',
            error: '',
            registering: false
        };
    }

    register = () => {
        this.setState({
            registering: true
        });

        let userPool = new CognitoUserPool(config.poolData);
        let attrList = [
            new CognitoUserAttribute({
                Name: 'email',
                Value: this.state.email
            })
        ];

        userPool.signUp(
            this.state.username,
            this.state.password,
            attrList,
            null,
            (err, res) => {
                if (err) {
                    console.error(err);
                    this.setState({
                        error: errorMessages[err.code]
                    });
                }
                else {
                    this.setState({
                        error: ''
                    });
                    console.log(res);
                }

                this.setState({
                    registering: false
                });
            });
    }

    setUsername = (event) => {
        this.setState({
            username: event.target.value
        });
    }

    setEmail = (event) => {
        this.setState({
            email: event.target.value
        });
    }

    setPassword = (event) => {
        this.setState({
            password: event.target.value
        });
    }

    render() {
        let buttonContents = [];
        if (this.state.registering) {
            buttonContents.push(
                <bpcore.Spinner
                    className="qdquarry__login-spinner"
                    size={bpcore.Spinner.SIZE_SMALL}
                    intent={bpcore.Intent.PRIMARY} />
            )
        } else {
            buttonContents.push('Submit');
        }

        let error = 
            this.state.error ? 
                (<bpcore.Callout
                    icon="error"
                    intent={bpcore.Intent.DANGER}
                    className="qdquarry__login-error-callout">
                    
                    {this.state.error}
                </bpcore.Callout>) :
                null;

        return (
            <div className="qdquarry__login-wrapper color-site-background-dark">
                <bpcore.Card
                    className="qdquarry__login-main-card color-site-background-light"
                    elevation={bpcore.Elevation.TWO}>

                    {error}

                    <bpcore.H4>Register a new QDQuarry account</bpcore.H4>

                    <bpcore.InputGroup
                        className="qdquarry__login-field"
                        placeholder="Username"
                        disabled={this.state.registering}
                        onChange={this.setUsername}
                        >

                    </bpcore.InputGroup>

                    <bpcore.InputGroup
                        className="qdquarry__login-field"
                        placeholder="Email"
                        disabled={this.state.registering}
                        onChange={this.setEmail}
                        >

                    </bpcore.InputGroup>

                    <bpcore.InputGroup
                        className="qdquarry__login-field"
                        placeholder="Password"
                        disabled={this.state.registering}
                        onChange={this.setPassword}
                        type="password">

                    </bpcore.InputGroup>

                    <bpcore.Button
                        className="qdquarry__login-submit-button"
                        disabled={this.state.registering}
                        onClick={this.register}>

                        {buttonContents}
                    </bpcore.Button>
                </bpcore.Card>
            </div>
        );
    }

}