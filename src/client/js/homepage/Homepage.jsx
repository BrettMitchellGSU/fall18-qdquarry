
/* jshint ignore:start */

import * as React from 'react';
import * as bpcore from '@blueprintjs/core';

export class Homepage extends React.PureComponent {

    navigate = (page) => {
        return () => { window.location.href = page; };
    }

    render() {
        return (
            <div className="qdquarry__homepage-wrapper color-site-background-dark">
                <bpcore.Card className="qdquarry__homepage-title-bar">
                    <div className="qdquarry__homepage-account-buttons">
                        <bpcore.Button
                            onClick={this.navigate('/login.html')}>
                            
                        Login
                        </bpcore.Button>
                        <bpcore.Button
                            onClick={this.navigate('/register.html')}>
                            
                        Register
                        </bpcore.Button>
                        <bpcore.Button
                            onClick={this.navigate('/change-password.html')}>
                            
                        Change Password
                        </bpcore.Button>
                    </div>
                </bpcore.Card>

                <div className="qdquarry__homepage-main-content">
                </div>
            </div>
        );
    }
}
