/* jshint ignore:start */

import * as React from 'react';
import * as bpcore from '@blueprintjs/core';

import { config } from '../common/CognitoClientConfig';
import { CognitoUserPool, AuthenticationDetails, CognitoUser } from 'amazon-cognito-identity-js';

const errorMessages = {
    'NotAuthorizedException': 'Incorrect username or password',
    'UserNotFoundException': 'That username does not exist',
    'InvalidParameterException': 'Please enter your username and password to log in',
    'UserNotConfirmedException': 'Your email address has not been confirmed yet'
}

export class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            loggingIn: false
        };
    }

    login = () => {
        this.setState({
            loggingIn: true
        });

        let userPool = new CognitoUserPool(config.poolData);

        let authDetails = new AuthenticationDetails({
            Username: this.state.username,
            Password: this.state.password
        });

        let user = new CognitoUser({
            Username: this.state.username,
            Pool: userPool
        });

        user.authenticateUser(authDetails, {
            onSuccess: (res) => {
                console.log(res);
            },
            onFailure: (err) => {
                console.log(err);
                this.setState({
                    error: errorMessages[err.code]
                })

                this.setState({
                    loggingIn: false
                });
            },
            newPasswordRequired: (userAttributes, requiredAttributes) => {
                window.location.href = '/change-password'
            }
        });
    }

    setUsername = (event) => {
        this.setState({
            username: event.target.value
        });
    }

    setPassword = (event) => {
        this.setState({
            password: event.target.value
        });
    }

    render() {
        let buttonContents = [];
        if (this.state.loggingIn) {
            buttonContents.push(
                <bpcore.Spinner
                    className="qdquarry__login-spinner"
                    size={bpcore.Spinner.SIZE_SMALL}
                    intent={bpcore.Intent.PRIMARY} />
            )
        } else {
            buttonContents.push('Submit');
        }

        let error = 
            this.state.error ? 
                (<bpcore.Callout
                    icon="error"
                    intent={bpcore.Intent.DANGER}
                    className="qdquarry__login-error-callout">
                    
                    {this.state.error}
                </bpcore.Callout>) :
                null;

        return (
            <div className="qdquarry__login-wrapper color-site-background-dark">
                <bpcore.Card
                    className="qdquarry__login-main-card color-site-background-light"
                    elevation={bpcore.Elevation.TWO}>

                    {error}

                    <bpcore.H4>Log in to your QDQuarry account</bpcore.H4>

                    <bpcore.InputGroup
                        className="qdquarry__login-field"
                        placeholder="Username"
                        disabled={this.state.loggingIn}
                        onChange={this.setUsername}
                        >

                    </bpcore.InputGroup>

                    <bpcore.InputGroup
                        className="qdquarry__login-field"
                        placeholder="Password"
                        disabled={this.state.loggingIn}
                        onChange={this.setPassword}
                        type="password">

                    </bpcore.InputGroup>

                    <bpcore.Button
                        className="qdquarry__login-submit-button"
                        disabled={this.state.loggingIn}
                        onClick={this.login}>

                        {buttonContents}
                    </bpcore.Button>
                </bpcore.Card>
            </div>
        );
    }

}