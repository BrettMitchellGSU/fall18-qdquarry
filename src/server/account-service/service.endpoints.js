
import { EndpointCollection } from '../util/EndpointCollection';

import { Register } from './endpoints/Register';
import { EditProfile } from './endpoints/EditProfile';

export const endpoints = new EndpointCollection(
    {
        post: [
            ['/register', Register],
            ['/edit-profile', EditProfile]
        ],

        get: []
    }
);
