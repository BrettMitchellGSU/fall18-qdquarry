
/* jshint ignore:start */

import { EndpointCollection } from '../util/EndpointCollection';

import { GetHomePage     } from './endpoints/GetHomePage';
import { GetLoginPage    } from './endpoints/GetLoginPage';
import { GetRegisterPage } from './endpoints/GetRegisterPage';
import { GetDashboard    } from './endpoints/GetDashboard';
import { GetWorkspace    } from './endpoints/GetWorkspace';
import { GetBundle       } from './endpoints/GetBundle';

export const endpoints = new EndpointCollection(
    {
        get: [
            ['/',                       GetHomePage     ],
            ['/login',                  GetLoginPage    ],
            ['/register',               GetRegisterPage ],
            ['/dashboard',              GetDashboard    ],
            ['/workspace/:workspaceID', GetWorkspace    ],
            ['/bundle/:bundleName',     GetBundle       ]
        ],

        post: []
    }
);
