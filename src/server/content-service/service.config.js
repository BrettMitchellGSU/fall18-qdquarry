
export const config = {
    port: 3000,

    htmlDirectory: '/code/SCHOOL/CSCI-5530/fall18-qdquarry/src/server/content-service/static/html/',
    bundleDirectory: '/code/SCHOOL/CSCI-5530/fall18-qdquarry/src/server/content-service/static/bundle/'
};
