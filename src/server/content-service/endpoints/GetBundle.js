
import fs from 'fs';

import { config } from '../service.config';

export const GetBundle = function(req, res) {

    const bundleName = req.params.bundleName;

    if (bundleName && fs.existsSync(config.bundleDirectory + bundleName)) {
        res.sendFile(config.bundleDirectory + bundleName);
    } else {
        res.status(404);
        res.send('Bundle ' + bundleName + ' not found');
    }
};
