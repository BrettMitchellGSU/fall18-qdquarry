
import { config } from '../service.config';

export const GetHomePage = function(req, res) {
    res.sendFile(config.htmlDirectory + 'homepage.html');
};
