
import { config } from '../service.config';

export const GetWorkspace = function(req, res) {
    res.sendFile(config.htmlDirectory + 'workspace.html');
};
