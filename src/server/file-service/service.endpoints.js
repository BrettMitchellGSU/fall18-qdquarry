
import * as multer from 'multer';

import { config } from './service.config';

const upload = multer({ dest: config.tempFileDirectory })

import { EndpointCollection } from '../util/EndpointCollection';

import { Upload   } from './endpoints/Upload';
import { Download } from './endpoints/Download';
import { Remove   } from './endpoints/Remove';

export const endpoints = new EndpointCollection(
    {
        post: [
            [ '/a/file/upload',   Upload, upload.single('file') ],
            [ '/a/file/download', Download ],
            [ '/a/file/remove',   Remove   ]
        ],

        get: []
    }
);
