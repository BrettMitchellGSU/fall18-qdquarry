
import { Service } from '../util/Service';
import { config } from './service.config';
import { endpoints } from './service.endpoints';

let service = new Service(config, endpoints);

service.start();
