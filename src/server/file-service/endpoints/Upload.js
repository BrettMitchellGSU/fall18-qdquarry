
/* jshint ignore:start */

import * as fs from 'fs';
import { promisify } from 'util';

const fsPromise = {
    readFile: promisify(fs.readFile),
    unlink: promisify(fs.unlink)
};

import moment from 'moment';

import { config } from '../../util/s3/s3.config';
import { s3 } from '../../util/s3/Interface';

import { listMissing, newFilePrefix, newHashKey } from '../../../common/helpers';

import { File } from '../../util/db/models/File';
import { Item } from '../../util/db/Item';

export const Upload = async function(req, res) {

    const {
        userID
    } = req.body;

    const file = req.file;

    const result = {success: false};

    const missing = listMissing({userID, file});
    if (missing !== '') {
        result.message = 'Missing parameters: ' + missing;
    } else {
    
        try {
            const dataBuffer = await fsPromise.readFile(file.path);

            if (!dataBuffer) {
                throw 'Filesystem error';
            }

            let filename = userID.replace('@', '_at_')
                                 .replace('.', '_dot_') +
                                 '/' + newFilePrefix() + file.originalname;

            let params = {
                Bucket: config.s3UserFileBucket,
                Key: filename,
                Body: dataBuffer
            };

            await s3.putObject(params).on('error', err => { console.error(err) }).promise();

            await fsPromise.unlink(file.path);

            let fileData = {
                file_id: newHashKey(),
                user_id: userID,
                filename: filename,
                bytes: file.size,
                upload: moment().format('YYYY-MM-DD hh:mm:ss')
            }

            let newFile = new Item({table: File, data: fileData});
            await newFile.put();

            result.success = true;
            result.message = 'Uploaded file ' + file.originalname + ' to user ' + userID + ' file collection'

        } catch (e) {
            console.log(e, e.stack);
            result.message = 'message' in e ? e.message : e;
        }

    }

    res.send(JSON.stringify(result));

};
