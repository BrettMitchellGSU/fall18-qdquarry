
/* jshint ignore:start */

import { config } from '../../util/s3/s3.config';
import { s3 } from '../../util/s3/Interface';

import { listMissing } from '../../../common/helpers';

import { File } from '../../util/db/models/File';

export const Download = async function(req, res) {

    const {
        fileID,
        userID,
    } = req.body;

    let result = {success: false};
    
    const missing = listMissing({fileID, userID});
    if (missing !== '') {
        result = {
            success: false,
            message: 'Missing parameters: ' + missing
        }
    } else {

        const file = await File.get()
                               .queryBy({hashKeyValue: fileID, sortKeyValue: userID})
                               .execute();
                            
        if (!file) {
            result.message = 'No file ' + fileID + ' for user ' + userID;
        } else if (!file.attr('filename')) {
            result.message = 'No S3 location for file ' + fileID + ' for user ' + userID;
        } else {

            const filename = file.attr('filename');

            const params = {
                Bucket: config.s3UserFileBucket,
                Key: filename
            };
        
            s3.getObject(params)
            .on('httpHeaders', function (statusCode, headers) {
                res.status(statusCode);
                res.set('Content-Length', headers['content-length']);
                res.set('Content-Type', headers['content-type']);
                this.response.httpResponse.createUnbufferedStream()
                    .pipe(res);
            })
            .on( 'error', e => console.log(e) )
            .send();

            return;
        }
    }

    if (!result.success)
        res.status(404);
    // If an error occured at any point, send the message in a JSON object.
    res.send(JSON.stringify(result));
};
