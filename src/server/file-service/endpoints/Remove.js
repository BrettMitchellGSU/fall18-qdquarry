
/* jshint ignore:start */

import { config } from '../../util/s3/s3.config';
import { s3 } from '../../util/s3/Interface';

import { listMissing } from '../../../common/helpers';

import { File } from '../../util/db/models/File';

export const Remove = async function(req, res) {

    const {
        fileID,
        userID
    } = req.body;

    let result = {success: false};

    const missing = listMissing({fileID, userID});
    if (missing !== '') {
        result.message = 'Missing parameters: ' + missing;
    } else {
        const file = await File.get()
                               .queryBy({hashKeyValue: fileID, sortKeyValue: userID})
                               .execute();
        
        if (!file) {
            result.message = 'No file ' + fileID + ' for user ' + userID;
        } else {
            const params = {
                Bucket: config.s3UserFileBucket,
                Key: file.attr('filename')
            };

            s3.deleteObject(params, err => {
                if (err) console.error(err);
            });

            file.delete();

            result.success = true;
            result.message = 'Deleted file ' + fileID + ' from user ' + userID + '\'s account personal storage';
        }
    }

    res.send(JSON.stringify(result));
};
