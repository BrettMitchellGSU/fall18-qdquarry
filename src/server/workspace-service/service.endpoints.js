
import { EndpointCollection } from '../util/EndpointCollection';

import { InviteUsers            } from './endpoints/InviteUsers';
import { RemoveUsers            } from './endpoints/RemoveUsers';
import { JoinWorkspaceSession   } from './endpoints/JoinWorkspaceSession';
import { LeaveWorkspaceSession  } from './endpoints/LeaveWorkspaceSession';
import { UpdateWorkspaceSession } from './endpoints/UpdateWorkspaceSession';

export const endpoints = new EndpointCollection(
    {
        post: [
            [ '/invite-users',   InviteUsers            ],
            [ '/remove-users',   RemoveUsers            ],
            [ '/join-ws-sess',   JoinWorkspaceSession   ],
            [ '/leave-ws-sess',  LeaveWorkspaceSession  ],
            [ '/update-ws-sess', UpdateWorkspaceSession ],
        ],

        get: []
    }
);
