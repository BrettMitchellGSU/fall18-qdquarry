
function getNestedEndpoints(routePrefix, endpoints) {
    let _endpoints = [];
    for (let endpoint of endpoints) {
        if (Array.isArray(endpoint[1]))
            _endpoints.push(
                ...getNestedEndpoints(routePrefix + endpoint[0], endpoint[1])
            );
        else {
            let newFlattenedEndpoint = [routePrefix + endpoint[0], endpoint[1]];
            if (endpoint.length > 2) newFlattenedEndpoint[2] = endpoint[2];
            _endpoints.push(newFlattenedEndpoint);
        }
    }

    return _endpoints;
}

export class EndpointCollection {
    /**
     * 
     * @param {Object} endpoints Must contain keys 'get' and 'post'. Formatted
     * as a nested list of tuples forming routes. Endpoints are functions taking
     * request and response objects and writing the appropriate
     */
    constructor(endpoints) {
        this.endpoints = endpoints;
    }

    allGetEndpoints() {
        return getNestedEndpoints('', this.endpoints.get);
    }

    allPostEndpoints() {
        return getNestedEndpoints('', this.endpoints.post);
    }
}

export class EndpointCollectionAccessor {
    constructor(endpoints) {
        this.endpoints = endpoints;
    }
}
