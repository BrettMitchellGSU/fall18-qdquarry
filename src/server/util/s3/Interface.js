
import * as aws from 'aws-sdk';

import { config } from './s3.config';

export const s3 = new aws.S3({
    apiVersion: '2006-03-01',
    accessKeyId: config.s3AccessKeyId,
    secretAccessKey: config.s3SecretAccessKey,
    region: config.s3Region
});
