
/* jshint ignore:start */

/**
 * Service class adapted from https://github.com/aws-samples/eb-node-express-sample/blob/master/app.js
 */

import * as express from 'express';
import * as cluster from 'cluster';
import * as multer  from 'multer' ;

// Taken from https://medium.com/@Abazhenov/using-async-await-in-express-with-node-8-b8af872c0016
// Allows asynch/await functionality in endpoints
const asyncMiddleware = fn =>
    (req, res, next) => {
        Promise.resolve(fn(req, res, next))
        .catch((err) => {
            console.error(err);
            next(err);
        });
};

const _set_app_endpoints = function(app, endpoints) {
    for (let endpoint of endpoints.allGetEndpoints())
        app.get(endpoint[0], asyncMiddleware(async (req, res, next) => {
            await endpoint[1](req, res);
        }));

    for (let endpoint of endpoints.allPostEndpoints())
        // If the post endpoint is set up with a multer middleware method, use
        // it. Otherwise, use a no-op middleware function.
        app.post(endpoint[0], endpoint[2] ? endpoint[2] : (req, res, next) => next(), asyncMiddleware(async (req, res, next) => {
            await endpoint[1](req, res);
        }));
}

const _assign_free_port = function(port, app) {
    let server = app.listen(port, function () {
        console.log('Server running at port ' + port + '');
    });
    server.on('error', () => { server.close(); _assign_free_port(++port, server); });
}

export class Service {
    constructor(config, endpoints) {
        this.port = config.port || 3000;
        this.endpoints = endpoints || { allGetEndpoints : () => [],
                                        allPostEndpoints: () => [] }
    }

    start() {
        // Code to run if we're in the master process
        if (cluster.isMaster) {
            // Count the machine's CPUs
            var cpuCount = require('os').cpus().length || 1;
        
            // Create a worker for each CPU
            for (let i = 0; i < cpuCount; i += 1) {
                cluster.fork();
            }
        
            // Listen for terminating workers
            cluster.on('exit', function (worker) {
        
                // Replace the terminated workers
                console.log('Worker ' + worker.id + ' died :(');
                cluster.fork();
        
            });
        
        // Code to run if we're in a worker process
        } else {
            let app = express();

            app.use(express.json());
            app.use(express.urlencoded({extended: true}));

            _set_app_endpoints(app, this.endpoints);
        
            let initialPort = this.port || 3000;

            _assign_free_port(initialPort, app);
        }
    }
}
