
/* jshint ignore:start */

import { Item } from './Item';
import { TYPES } from './Table';
import { db } from './Interface';

export class GetBuilder {

    constructor(table) {
        this._use_batch = false;
        this._lock_batch = false;
        this.table = table;
        this.attrMode = 'exclude';
        this.excludedAttributes = [];
        this.includedAttributes = [];
        this.values = [];

        this.hashKey = table.attributes.find(v => v.hashKey);
        if (!this.hashKey) console.error('No hash key found for table ' + table.name);
        this.sortKey = table.attributes.find(v => v.sortKey);

        this._executeMode = 'get';
    }

    useBatch(b) {
        if (!this._lock_batch)
            this._use_batch = b;
    }

    excludeAttributes(attrs) {
        this.attrMode = 'exclude';
        this.excludedAttributes = attrs;
        return this;
    }

    includeAttributes(attrs) {
        this.attrMode = 'include';
        this.includedAttributes = attrs;
        return this;
    }

    executeMode(mode) {
        this._executeMode = mode;
        return this;
    }

    queryBy(values) {

        let _values = values;
        
        // Allows us to pass a single hash key value
        if (!Array.isArray(_values))
            _values = [_values];
        
        // Allows us to pass a single value pair
        if (!Array.isArray(_values[0]) && typeof _values[0] !== 'object')
            _values = [_values];

        if (_values.length > 1) {
            this._lock_batch = true;
            this._use_batch = true;
        }

        this.values = _values;

        return this;
    }

    /**
     * Builds the parameters for a call to DynamoDB's query method.
     * TODO: Support advanced features such as inequality operators on sort keys
     */
    buildQueryKey() {

        let v = this.values[0];
        let hkv = (Array.isArray(v) ? v[0] : v.hashKeyValue);

        let expressionAttributeValues = {};
        expressionAttributeValues[':a'] = {};
        expressionAttributeValues[':a'][this.hashKey.type] = hkv;

        let keyConditionExpression = this.hashKey.name + ' = :a';

        return {
            ExpressionAttributeValues: expressionAttributeValues,
            KeyConditionExpression: keyConditionExpression
        };

    }

    buildGetKeys() {
        let keys = [];
        
        for (let v of this.values) {
            let k = {};
            k[this.hashKey.name] = {};
            let castValue = TYPES.CAST((Array.isArray(v)  ? v[0] : v.hashKeyValue), this.hashKey.type);
            k[this.hashKey.name][this.hashKey.type] = castValue;
            if (this.sortKey) {
                let value = Array.isArray(v) ? v[1] : v.sortKeyValue;
                if (this._executeMode === 'get' && !value) {
                    console.error('Sort key not given for "get" mode execute operation');
                    return this;
                }
                k[this.sortKey.name] = {};
                castValue = TYPES.CAST(value, this.sortKey.type);
                k[this.sortKey.name][this.sortKey.type] = castValue;
            }
            keys.push(k);
        }

        return keys;
    }

    buildProjectionExpression() {
        return this.table.attributes
        .filter(
            this.attrMode === 'include' ?
            a => !this.excludedAttributes.includes(a.name) :
            a =>  this.includedAttributes.includes(a.name)
        ).map(
            a => a.name
        ).join(', ');
    }

    buildGetParams() {
        let projectionExpression = this.buildProjectionExpression();

        let params = {};

        if (this._use_batch) {
            params.RequestItems = {};
            params.RequestItems[this.table.name] = {
                Keys: this.buildGetKeys()
            };
            if (!!projectionExpression)
                params.RequestItems[this.table.name].ProjectionExpression = projectionExpression;
        } else {
            params.Key = this.buildGetKeys()[0];
            params.TableName = this.table.name;
            if (!!projectionExpression)
                params.ProjectionExpression = projectionExpression;
        }

        return params;
    }

    buildQueryParams() {
        let projectionExpression = this.buildProjectionExpression();

        let params = {
            ...this.buildQueryKey(),
            TableName: this.table.name
        };

        if (!!projectionExpression)
            params.ProjectionExpression = projectionExpression;

        return params;
    }

    execute = async () => {
        if (this._executeMode === 'get') {
            if (this._use_batch) {
                let items = [];
                let data = await db.batchGetItem(this.buildGetParams()).promise();
                if (Object.keys(data).length) {
                    data.Responses[this.table.name].map( v => {
                        items.push(new Item({table: this.table, data: Item.formatItemData(v, this.table)}));
                    });
                }
                return items;
            }
            else {
                let item = null;
                let data = await db.getItem(this.buildGetParams()).promise();
                if (Object.keys(data).length) {
                    item = new Item({table: this.table, data: Item.formatItemData(data.Item, this.table)});
                }
                return item;
            }
        } else if (this._executeMode === 'query') {
            let items = [];
            let data = await db.query(this.buildQueryParams()).promise();
            if (Object.keys(data).length) {
                data.Items.map( v => {
                    items.push( new Item( { table: this.table, data: Item.formatItemData( v, this.table ) } ) );
                });
            }
            return items;
        } else {
            console.error('Invalid execute mode: ' + this._executeMode);
        }
    }

}
