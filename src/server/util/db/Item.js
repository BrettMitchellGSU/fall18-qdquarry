
import { Table } from './Table';

export class Item {

    /**
     * @param {Table} table The table definition used to provide structure to
     * the item's attributes.
     * @param {Map} data The data to initialize the instance with. If any
     * field is not provided, that attribute's default value is assigned.
     */
    constructor({table, data = {}}) {
        this.table = table;

        // Get default values for all attributes
        this.attributes = table.getAttributeDefaults();

        // Fill out any attribute values given in data
        this.attributes = this.attributes.map(
            v => (v.name in data) ?
                  { name: v.name, value: data[v.name] }
                : v
        );
    }

    /**
     * Gets or sets the attribute with name 'attributeName'
     * @param {String} attributeName Name of the attribute to get or set
     * @param {any} value [Optional] Value to set the attribute to
     */
    attr(attributeName, value) {
        let attr = this.attributes.find(v => v.name === attributeName);
        if (value !== undefined &&
           (!attr.required || value !== null)) {

            attr.value = value;
            attr.changed = true;
        } else {
            return attr.value;
        }
    }

    /**
     * Create a DynamoDB compatible object representing this item.
     * @param {any => boolean} filter The filter function determines which
     * attributes will be included in the final object. Not passing a filter
     * function defaults to all attributes.
     */
    objectify(filter) {
        // Default behavior is to objectify all fields
        if (!filter) filter = _ => true;

        let oa = {};
        let filteredAttributes = this.table.attributes.filter(filter);

        for (let a of filteredAttributes) {
            let value = this.attributes.find(ia => ia.name === a.name).value;
            Object.assign(oa, a.objectify(value));
        }

        return oa;
    }

    /**
     * Create a new item with this item's attribute values in the database
     */
    put() {
        /*let params = {
            TableName: this.constructor.table.tableName,
            Item: this.objectifyAttributes()
        };

        db.putItem(params);*/

        this.table.put(this);
    }

    /**
     * Delete this item
     */
    delete() {
        /*let params = {
            TableName: this.constructor.table.tableName,
            Key: this.objectifyAttributes(
                v => {
                    let a = this.table.attributes[v.name];
                    return a.hashKey || a.sortKey;
                }
            )
        };

        db.deleteItem(params);*/

        this.table.delete(this);
    }

    /**
     * Update this item
     */
    update() {
        /*let params = {
            TableName: this.constructor.table.tableName
        };*/

        this.table.update(this);
    }

}

Item.formatItemData = function(data, table) {
    let formatted = {};
    for (let attrName in data) {
        let attrType = table.attr(attrName).type;
        formatted[attrName] = data[attrName][attrType];
    }
    return formatted;
};
