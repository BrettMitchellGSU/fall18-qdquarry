
import { DynamoDB, Endpoint } from 'aws-sdk';
import { config } from './dynamo.config';

const params = {
    accessKeyId: config.dynamoDBaccessKeyId,
    secretAccessKey: config.dynamoDBsecretAccessKey,
    endpoint: new Endpoint( config.dynamoDBEndpoint[config.mode] ),
    region: config.dynamoDBregion
};

export const db = new DynamoDB( params );
