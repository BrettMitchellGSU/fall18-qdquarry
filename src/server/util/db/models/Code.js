
import { Attribute, Table, TYPES } from '../Table';

export const Code = new Table({
    name: 'codes',
    attributes: [
        new Attribute({
            name: 'code_id',
            type: TYPES.STRING,
            hashKey: true,
            required: true
        }),
        new Attribute({
            name: 'workspace_id',
            type: TYPES.STRING,
            sortKey: true,
            required: true
        }),
        new Attribute({
            name: 'color',
            type: TYPES.STRING,
            required: true
        }),
        new Attribute({
            name: 'segments',
            type: TYPES.LIST,
            required: true,
            default: []
        }),
        new Attribute({
            name: 'tags',
            type: TYPES.STRING_SET,
            required: true,
            default: []
        })
    ]
});
