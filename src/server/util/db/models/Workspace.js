
import { Table, Attribute, TYPES } from "../Table";

export const Workspace = new Table({
    name: 'workspaces',
    attributes: [
        new Attribute({
            name: 'workspace_id',
            type: TYPES.STRING,
            required: true,
            hashKey: true
        }),
        new Attribute({
            name: 'owner_id',
            type: TYPES.STRING,
            sortKey: true,
            required: true
        }),
        new Attribute({
            name: 'name',
            type: TYPES.STRING,
            required: true
        }),

        new Attribute({
            name: 'members',
            type: TYPES.LIST,
            defaultValue: [],
            required: true
        }),
        new Attribute({
            name: 'session_members',
            type: TYPES.LIST,
            defaultValue: [], 
            required: true
        }),
        new Attribute({
            name: 'last_update_hash',
            type: TYPES.STRING
        }),

        new Attribute({
            name: 'code_hierarchy',
            type: TYPES.MAP,
            defaultValue: {},
            required: true
        }),
        new Attribute({
            name: 'file_hierarchy',
            type: TYPES.MAP,
            defaultValue: {},
            required: true
        })
    ]
});
