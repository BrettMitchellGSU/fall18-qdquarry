
import { Attribute, Table, TYPES } from '../Table';

export const Dashboard = new Table({
    name: 'dashboards',
    attributes: [
        new Attribute({
            name: 'user_id',
            type: TYPES.STRING,
            hashKey: true,
            required: true
        }),
        new Attribute({
            name: 'file_hierarchy',
            type: TYPES.MAP,
            required: true,
            default: {}
        }),
        new Attribute({
            name: 'workspace_hierarchy',
            type: TYPES.MAP,
            required: true,
            default: {}
        })
    ]
});
