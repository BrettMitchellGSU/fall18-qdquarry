
import { Attribute, Table, TYPES } from '../Table';

export const File = new Table({
    name: 'files',
    attributes: [
        new Attribute({
            name: 'file_id',
            type: TYPES.STRING,
            hashKey: true,
            required: true
        }),
        new Attribute({
            name: 'user_id',
            type: TYPES.STRING,
            sortKey: true,
            required: true
        }),

        new Attribute({
            name: 'filename',
            type: TYPES.STRING,
            required: true
        }),

        new Attribute({
            name: 'bytes',
            type: TYPES.NUMBER
        }),
        
        new Attribute({
            name: 'upload',
            type: TYPES.STRING,
            required: true
        })
    ]
});
