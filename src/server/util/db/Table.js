
/* jshint ignore:start */

import { db } from './Interface';

import { GetBuilder } from './GetBuilder';
import { Item } from './Item';

const BATCH_PUT_LIMIT = 25;
const BATCH_GET_LIMIT = 100;

/**
 * Split an array in to chunks
 * @param {Array} arr The array to split
 * @param {Number} chunkSize Maximum number of elements per chunk
 */
function chunkArray(arr, chunkSize) {
    let chunked = [];

    for (let i = 0; i < arr.length; i += chunkSize) 
        chunked.push(arr.slice(i, i + chunkSize));

    return chunked;
}

/**
 * Parses a list of Items and determines whether or not they require operations
 * which are invalid for a batch write operation.
 * @param  {...Item} items Items to check for batch writing validity.
 */
function canBatchWrite(...items) {
    return items.length > 1;
}

/**
 * Represents a DynamoDB table. Contains rules for which attributes can be
 * assigned to items and CRUD methods for interacting with the table.
 */
export class Table {

    /**
     * Creates a new table definition for a given DynamoDB table with the given
     * attribute definitions.
     * @param {string} name The name of the table in DynamoDB
     * @param {Array<Attribute>} attributes Definitions of the attributes of the
     * table.
     */
    constructor({name, attributes, throughput = {read: 5, write: 5}}) {
        this.name = name;
        this.attributes = attributes;
        this.throughput = throughput;

        this.hashKey = attributes.find(a => a.hashKey);
        this.sortKey = attributes.find(a => a.sortKey);
    }

    /**
     * Called on exiting from a put operation
     * @param {Error} err Any error which may have occured while performing the
     * put operation.
     * @param {any} data Any data which may have been returned on completing the
     * put operation
     */
    putCallback(err, data) {
        if (err) console.error(err);
    }

    /**
     * Puts a list of Items into the database. Uses DynamoDB's batchWriteItem
     * operation if possible. Otherwise, this method puts each item
     * individually.
     * @param  {...Item} items Items to be put into the table
     */
    put(...items) {
        if (!canBatchWrite(items)) {
            items.forEach(i => this.individualPut(i));
            return;
        }

        let itemChunks  = chunkArray(items, BATCH_PUT_LIMIT);
        let params = [];

        for (let itemChunk of itemChunks) {
            for (let item of itemChunk) {
                if (item instanceof Item) {
                    params.push({ PutRequest: { Item: item.objectify() } });
                }
            }

            params = { RequestItems: { [this.name]: params } };

            db.batchWriteItem(params, this.putCallback);

            params = [];
        }
    }

    /**
     * Non-batch put operation
     * @param  {...Item} items Items to put
     */
    individualPut(item) {
        let params = {
            Item: item.objectify(),
            TableName: this.name
        };

        db.putItem(params, this.putCallback);
    }

    /**
     * Returns a builder object for creating and executing get operations with.
     */
    get() {
        return new GetBuilder(this);
    }

    /**
     * Updates a list of items. DynamoDB does not support updating with
     * batchWriteItem, so each item must be individually updated with updateItem
     * @param  {...Item} items The items to update
     */
    update(...items) {
        for (let item of items) {
            if (item instanceof Item) {
                this.individualUpdate(item);
            } else {
                console.error('Got non-item in Table.update');
            }
        }
    }

    /**
     * Sends changed attributes on item to DynamoDB
     * @param {Item} item The item to update
     */
    individualUpdate(item) {
        let params = {};
        let markerIdx = 0;
        let changedAttributes = item.attributes
                                    .filter(a => a.changed)
                                    .map(a => { return {
                                                    nameMarker: '#Attr' + ++markerIdx,
                                                    name: a.name,
                                                    valueMarker: ':AttrVal' + markerIdx,
                                                    value: a.value,
                                                    type: this.attributes.find(tableA => tableA.name === a.name).type
                                                };
                                            });
        params.UpdateExpression = 'SET ' + changedAttributes.map(a => a.nameMarker + ' = ' + a.valueMarker)
                                                            .join(', ');
        
        params.ExpressionAttributeNames = {};
        changedAttributes.forEach(
            a => params.ExpressionAttributeNames[a.nameMarker] = a.name
        );
        params.ExpressionAttributeValues = {};
        changedAttributes.forEach(
            a => params.ExpressionAttributeValues[a.valueMarker] = { [a.type]: a.value }
        );

        params.Key = {
            [this.hashKey.name]: { [this.hashKey.type]: item.attr(this.hashKey.name) },
            ...(this.sortKey ? {[this.sortKey.name]: { [this.sortKey.type]: item.attr(this.sortKey.name) }} : {})
        };

        params.TableName = this.name;
        
        db.updateItem(params, (err, data) => {
            if (err) {
                console.error(err);
                console.error(err.stack);
            }
        });
    }

    deleteKey(item) {
        return {
            ...this.hashKey.objectify(item.attr(this.hashKey.name)),
            ...this.sortKey ? 
                this.sortKey.objectify(item.attr(this.sortKey.name)) :
                {}
        };
    }

    /**
     * Deletes a list of items from the database. Uses DynamoDB's batchWriteItem
     * operation if possible. Otherwise, this method puts each item
     * individually.
     * @param  {...any} items The items to delete
     */
    delete(...items) {
        if (!canBatchWrite(items)) {
            for (const i of items)
                this.individualDelete(i);
            return;
        }

        let itemChunks  = chunkArray(items, BATCH_PUT_LIMIT);
        let params = [];

        for (let itemChunk of itemChunks) {
            for (let item of itemChunk) {
                if (item instanceof Item) {
                    params.push({
                        DeleteRequest: {
                            Key: this.deleteKey(item)
                        }
                    })
                }
            }

            params = { RequestItems: { [this.name]: params } };
            
            db.batchWriteItem(params, this.deleteCallback);

            params = [];
        }
    }

    individualDelete(item) {
        const params = {
            TableName: this.name,
            Key: this.deleteKey(item)
        }

        db.deleteItem(params, err => {
            if (err) console.error(err);
        });
    }

    attr(attr) {
        return this.attributes.find(v => v.name === attr);
    }

    getAttributeDefaults() {
        let ad = [];

        for (let a of this.attributes)
            ad.push({
                name: a.name,
                value: a.defaultValue(),
                changed: false
            });

        return ad;
    }

}

export const TYPES = {
    BOOLEAN: 'BOOL',
    BINARY: 'B',
    BINARY_SET: 'BS',
    NUMBER: 'N',
    NUMBER_SET: 'NS',
    STRING: 'S',
    STRING_SET: 'SS',
    NULL: 'NULL',
    LIST: 'L',
    MAP: 'M',

    // Custom
    BIG_STRING: 'BIG_STRING',
    
    CAST: function(d, t) {
        if (d === undefined || d === null) return null;
        switch (t) {
            case this.BOOLEAN:
                return !!d;
            case this.BINARY:
                return typeof d === 'string' ? d : d.toString();
            case this.BINARY_SET:
                return (Array.isArray(d) ? d : [d]).map(_d => _d.toString());
            case this.NUMBER:
                return typeof d === 'number' ? d : Number(d);
            case this.NUMBER_SET:
                return (Array.isArray(d) ? d : [d]).map(_d => Number(_d));
            case this.STRING:
                return d.toString();
            case this.STRING_SET:
                return (Array.isArray(d) ? d : [d]).map(_d => _d.toString());
            case this.NULL:
                return d ? true : false;
            case this.LIST:
                return Array.isArray(d) ? d : [d];
            case this.MAP:
                return typeof d === 'object' ? d : {value: d};
            default:
                console.error('Invalid type passed to TYPES.CAST + ' + t);
                return null;
        }
    },

    SUBMIT_CAST: function(d, t) {
        if (d === undefined || d === null) return null;
        switch (t) {
            case this.BOOLEAN:
                return !!d.toString();
            case this.BINARY:
                return d.toString()
            case this.BINARY_SET:
                return (Array.isArray(d) ? d : [d]).map(_d => _d.toString());
            case this.NUMBER:
                return d.toString()
            case this.NUMBER_SET:
                return (Array.isArray(d) ? d : [d]).map(_d => _d.toString());
            case this.STRING:
                return d.toString();
            case this.STRING_SET:
                return (Array.isArray(d) ? d : [d]).map(_d => _d.toString());
            case this.NULL:
                return d ? true : false;
            case this.LIST:
                return Array.isArray(d) ? d : [d];
            case this.MAP:
                return typeof d === 'object' ? d : {value: d};
            default:
                console.error('Invalid type passed to TYPES.SUBMIT_CAST + ' + t);
                return null;
        }
    }
};

export class Attribute {
    
    constructor({
        name,
        type = null,
        required,
        // Defines this attribute to be the hash key. Must be the only attribute
        // to have hashKey = true on the Schema.
        hashKey = false,
        // Defines this attribute to be a range key
        sortKey = false,
        // Sets the local or global secondary index options for DynamoDB
        index,
        // Default value to assign when no value is provided. If a function is
        // passed, it is called and the result is used as the default value
        defaultValue = null,
        // incomingTransform is applied when writing to the database
        incomingTransform = v => v,
        // outgoingTransform is applied when reading from the database
        outgoingTransform = v => v
    }) {
        this.name = name;
        this.type = type;
        this.required = required || hashKey || sortKey;
        this.hashKey = hashKey;
        this.sortKey = sortKey;
        this.index = index;
        this.defaultValue = function() {
            return typeof defaultValue === 'function' ?
                defaultValue() : defaultValue;
        };
        this.incomingTransform = incomingTransform;
        this.outgoingTransform = outgoingTransform;
    }

    objectify(value) {
        var objectified = {
            [this.name]: {
                [this.type]: TYPES.SUBMIT_CAST(value ? value : this.defaultValue(), this.type)
            }
        }
        return objectified;
    }

}
