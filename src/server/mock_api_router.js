
/* jshint ignore:start */

const express = require('express');
const axios = require('axios');
const app = express();
const path = require('path');

const call = function(domain, action, args = {}, method) {
    let header = (method === 'POST' ? {'Content-Type': 'application/json'} :
                                      {})
    // Return the ajax call to let the caller set .done, .fail, .always
    return axios({
        url: domain + action,
        method: method,
        headers: {
            ...header
        },
        data: args
    });
}

function _call(req, res, domain, method='POST') {
    let called = call(domain, req.params.action, req.body, method);

    let contentType = (method === 'GET' ? {'Content-Type': 'text/css'} :
                                           {})

    if (called) {
        called
        .then(function(response) {
            // simulate network latency with timeout
            setTimeout(function() {
                res.set(response.headers);
                res.send(response.data);
            }, 50);
        })
        .catch(function(err) {
            res.send(JSON.stringify({failed: true}));
        });
    } else {
        res.send(JSON.stringify({failed: true}));
    }
}

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post('/a/workspace/:action', function(req, res) {
    _call(req, res, 'http://localhost:3400/a/workspace/');
});

app.post('/a/file/:action', function(req, res) {
    _call(req, res, 'http://localhost:3200/a/file/');
});

app.post('/a/code/:action', function(req, res) {
    _call(req, res, 'http://localhost:3100/a/code/');
});

app.post('/a/hierarchy/:action', function(req, res) {
    _call(req, res, 'http://localhost:3300/a/hierarchy/');
});

/*app.post('/a/account/:action', function(req, res) {
    _call(req, res, 'http://localhost:3500/a/account/');
});*/

app.get('/:action', function(req, res) {
    _call(req, res, 'http://localhost:3000/', 'GET');
});

app.get('/:intermediate/:action', function(req, res) {
    _call(req, res, 'http://localhost:3000/' + req.params.intermediate + '/', 'GET');
});

app.listen(8080, function() { console.log('Listening on port 8080'); });
