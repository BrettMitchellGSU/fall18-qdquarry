
import { EndpointCollection } from '../util/EndpointCollection';

import { requestAuthEndpoint } from './endpoints/requestAuth';
import { requestRegisterEndpoint } from './endpoints/requestRegister';

export const endpoints = new EndpointCollection(
    {
        post: [
            ['/auth', requestAuthEndpoint],
            ['/register', requestRegisterEndpoint]
        ],

        get: []
    }
);
