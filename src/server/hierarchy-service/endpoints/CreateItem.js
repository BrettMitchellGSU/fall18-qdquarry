
/* jshint ignore:start */

import { listMissing } from '../../util/helpers';

import { doScopedHierarchyMethod, traverseHierarchy } from '../util/helpers';

const insertInHierarchy = function(hieararchy, result, {itemPath, item}) {
    let folder = traverseHierarchy(hieararchy, itemPath);

    if (!folder) {
        result.message = 'Path ' + itemPath + ' is not valid';
    } else {
        if (item.type === 'folder') {
            folder[item.name] = {M: {}};
        } else if (item.type === 'item') {
            folder[item.name] = {S: item.data};
        }

        result.success = true;
        result.message = 'Created ' + itemPath + '/' + item.name;
    }
}

export const CreateItem = async function(req, res) {

    let {
        scope,
        scopeID,
        type,
        itemPath,
        itemName,
        itemType,
        itemData
    } = req.body;

    let result = {success: false};

    let missing = listMissing({scope, scopeID, type, itemPath, itemName});
    if (missing !== '') {
        result = {
            success: false,
            message: 'Missing parameters: ' + missing
        }
    } else {
        result = await doScopedHierarchyMethod(scope, scopeID, type, insertInHierarchy, 
                                              {itemPath, item: {name: itemName, 
                                               data: itemData, type: itemType}});
    }

    res.send(JSON.stringify(result));
};
