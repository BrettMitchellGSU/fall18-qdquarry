
import { listMissing } from '../../util/helpers';

import { traverseHierarchy, doScopedHierarchyMethod } from '../util/helpers';

/* jshint ignore:start */

const deleteFromHierarchy = function(hierarchy, result, {itemPath, itemName}) {
    let folder = traverseHierarchy(hierarchy, itemPath);

    if (!folder) {
        result.message = 'Path ' + itemPath + ' is not valid';
    } else if (!folder[itemName]) {
        result.message = 'Item ' + itemName + ' not found';
    } else {
        delete folder[itemName];

        result.success = true;
        result.deleted = itemPath + '/' + itemName;
    }
}

export const DeleteItem = async function(req, res) {

    let {
        scope,
        scopeID,
        type,
        itemPath,
        itemName
    } = req.body

    let result = {success: false};

    let missing = listMissing({scope, scopeID, type, itemPath, itemName});
    if (missing !== '') {
        result.message = 'Missing parameters: ' + missing
    } else {
        result = await doScopedHierarchyMethod(scope, scopeID, type, deleteFromHierarchy,
                                              {itemPath, itemName});
    }

    res.send(JSON.stringify(result));
};
