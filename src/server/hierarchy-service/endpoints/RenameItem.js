
/* jshint ignore:start */

import { listMissing } from '../../util/helpers';

import { doScopedHierarchyMethod, traverseHierarchy } from '../util/helpers';

const renameInHierarchy = function(hierarchy, result, {itemPath, oldName, newName}) {
    let folder = traverseHierarchy(hierarchy, itemPath);

    if (!folder) {
        result.message = 'Path ' + itemPath + ' is not valid';
    } else if (!folder[oldName]) {
        result.message = 'Item ' + oldName + ' not found';
    } else {
        let val = folder[oldName];
        delete folder[oldName];
        folder[newName] = val;
    
        result.renamed = itemPath + '/' + oldName + ' -> ' + newName;
    }
};

export const RenameItem = async function(req, res) {

    let {
        scope,
        scopeID,
        type,
        itemPath,
        oldName,
        newName
    } = req.body;

    let result = {};

    let missing = listMissing({scope, scopeID, type, itemPath, oldName, newName});
    if (missing !== '') {
        result = {
            success: false,
            message: 'Missing parameters: ' + missing
        }
    } else {
        result = await doScopedHierarchyMethod(scope, scopeID, type, renameInHierarchy,
                                               {itemPath, oldName, newName});
    }

    res.send(JSON.stringify(result));
};
