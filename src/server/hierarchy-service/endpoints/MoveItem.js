
/* jshint ignore:start */

import { listMissing } from '../../util/helpers';

import { doScopedHierarchyMethod, traverseHierarchy } from "../util/helpers";

const moveInHierarchy = function(hierarchy, result, {oldItemPath, newItemPath, itemName}) {
    
    // Prevent a folder from being moved into itself
    if (oldItemPath + '/' + itemName === newItemPath) {
        result.success = false;
        result.message = 'Can\'t move a folder into itself: Tried to move ' + oldItemPath + '/' + itemName + ' -> ' + newItemPath;
        return;
    }

    let oldFolder = traverseHierarchy(hierarchy, oldItemPath);
    let newFolder = traverseHierarchy(hierarchy, newItemPath);

    if (!oldFolder) {
        result.message = 'Old path ' + oldItemPath + ' not valid';
    } else if (!newFolder) {
        result.message = 'New path ' + newItemPath + ' not valid';
    } else if (!oldFolder[itemName]) {
        result.message = 'Item ' + itemName + ' not found in path ' + oldItemPath;
    } else if (newFolder[itemName] !== undefined) {
        result.message = 'Item ' + itemName + ' already exists in path ' + newItemPath;
    } else {
        // Move the item
        let item = oldFolder[itemName];
        newFolder[itemName] = item;
        delete oldFolder[itemName];

        result.success = true;
        result.message = 'Moved ' + oldItemPath + '/' + itemName + ' -> ' + newItemPath + '/' + itemName;
    }
}

export const MoveItem = async function(req, res) {

    let {
        scope,
        scopeID,
        type,
        oldItemPath,
        newItemPath,
        itemName
    } = req.body;

    let result = {};

    let missing = listMissing({scope, scopeID, type, oldItemPath, newItemPath, itemName});
    if (missing !== '') {
        result = {
            success: false,
            message: 'Missing parameters: ' + missing
        }
    } else {
        result = await doScopedHierarchyMethod(scope, scopeID, type, moveInHierarchy,
                                               {oldItemPath, newItemPath, itemName});
    }

    res.send(JSON.stringify(result));
};
