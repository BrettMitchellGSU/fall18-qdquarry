
/* jshint ignore:start */

import { listMissing } from '../../util/helpers';

import { doScopedHierarchyMethod } from '../util/helpers';

const getHierarchy = function(hierarchy, result) {
    result.success = true;
    result.hierarchy = hierarchy;
}

export const GetHierarchy = async function(req, res) {

    let {
        scope,   // dashboard | workspace
        type,    // dashboard {file|workspace|all} | workspace {file|code|all}
        scopeID // id of the workspace or dashboard
    } = req.body;

    let result = {};

    // Enforce parameters
    let missing = listMissing({scope, type, scopeID});
    if (missing !== '') {
        result.message = 'Missing parameter: ' + missing;
    } else {
        result = await doScopedHierarchyMethod(scope, scopeID, type, getHierarchy)
    }
    
    res.send(JSON.stringify(result));
};
