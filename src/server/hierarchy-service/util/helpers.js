
/* jshint ignore:start */

import { Dashboard } from '../../util/db/models/Dashboard';
import { Workspace } from '../../util/db/models/Workspace';

// Traverses a hierarchy in DynamoDB object format. Returns the map level
// pointed to by path.
export const traverseHierarchy = function(hierarchy, path) {
    let splitPath = path
                    .split('/')
                    .map(v => v.trim())
                    .filter(v => !!v.length);

    let map_lvl = hierarchy;
    
    while (splitPath.length) {
        let pathElement = splitPath.shift();
        // Find the path element in the current map level
        map_lvl = map_lvl[pathElement];
        if (!map_lvl) return null;

        // Access the actual data item at the location 'pathElement'
        map_lvl = map_lvl[Object.keys(map_lvl)[0]];
        if (!map_lvl) return null;
    }

    return map_lvl;
};

// Performs an operation on a hierarchy.
export const doScopedHierarchyMethod = async function(scope, scopeID, type, method, {...otherArgs}) {
    let result = {success: false};

    if (scope === 'dashboard') {
        let dashboard = await Dashboard.get()
                                       .queryBy(scopeID)
                                       .execute();
        
        if (!dashboard) {
            result.message = 'No dashboard associated with that id';
        } else {
            let hierarchyAttribute = type === 'file' ? 'file_hierarchy' : 
                                     type === 'workspace' ? 'workspace_hierarchy' : '';
                                     
            let hierarchy = dashboard.attr(hierarchyAttribute);
            method(hierarchy, result, otherArgs);
            dashboard.attr(hierarchyAttribute, hierarchy);
            dashboard.update();
        }
        
    } else if (scope === 'workspace') {
        let workspace = (await Workspace.get()
                                        .executeMode('query')
                                        .queryBy(scopeID)
                                        .execute())[0];

        if (!workspace) {
            result.message = 'No workspace associated with that id';
        } else {
            let hierarchyAttribute = type === 'file' ? 'file_hierarchy' : 
                                     type === 'code' ? 'code_hierarchy' : '';

            let hierarchy = workspace.attr(hierarchyAttribute);
            method(hierarchy, result, otherArgs);
            workspace.attr(hierarchyAttribute, hierarchy);
            workspace.update();
        }
    }
    
    return result;
}
