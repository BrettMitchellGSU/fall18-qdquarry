
import { EndpointCollection } from '../util/EndpointCollection';

import { CreateItem   } from './endpoints/CreateItem';
import { DeleteItem   } from './endpoints/DeleteItem';
import { GetHierarchy } from './endpoints/GetHierarchy';
import { MoveItem     } from './endpoints/MoveItem';
import { RenameItem   } from './endpoints/RenameItem';

export const endpoints = new EndpointCollection(
    {
        post: [
            [ '/a/hierarchy/create-item',   CreateItem   ],
            [ '/a/hierarchy/delete-item',   DeleteItem   ],
            [ '/a/hierarchy/get-hierarchy', GetHierarchy ],
            [ '/a/hierarchy/move-item',     MoveItem     ],
            [ '/a/hierarchy/rename-item',   RenameItem   ],
        ],

        get: []
    }
);
