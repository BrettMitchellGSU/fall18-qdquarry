
/* jshint ignore:start */

import { listMissing } from '../../../common/helpers';

import { Code } from '../../util/db/models/Code';

export const RemoveSegment = async function(req, res) {

    let {
        codeID,
        workspaceID,
        segmentIndex
    } = req.body;

    let result = {success: false};

    let missing = listMissing({codeID, workspaceID, segmentIndex});
    if (missing !== '') {
        result = {
            success: false,
            message: 'Missing parameters: ' + missing
        };
    } else {
        
        let code = await Code.get()
                             .executeMode('get')
                             .queryBy({hashKeyValue: codeID,
                                       sortKeyValue: workspaceID})
                             .execute();
        
        if (!code) {
            result.message = 'No code associated with code ' + codeID + ' in workspace ' + workspaceID;
        } else {
            let segments = code.attr('segments');

            if (segmentIndex > segments.length || segmentIndex < 0) {
                result.message = 'Invalid segment index ' + segmentIndex + ' for segment list of size ' + segments.length;
            } else {
                delete segments[segmentIndex];
                code.attr('segments', segments);
                code.update();
        
                result.success = true;
                result.message = 'Removed segment ' + segmentIndex + ' from code ' + codeID + ' in workspace ' + workspaceID;
            }
        }
    }

    res.send(JSON.stringify(result));
}

