
/* jshint ignore:start */

import { listMissing } from '../../../common/helpers';

import { Code } from '../../util/db/models/Code';

export const RemoveTag = async function(req, res) {

    let {
        codeID,
        workspaceID,
        tag
    } = req.body;

    let result = {success: false};

    let missing = listMissing({codeID, workspaceID, tag});
    if (missing !== '') {
        result = {
            success: false,
            message: 'Missing parameters: ' + missing
        };
    } else {
        
        let code = await Code.get()
                             .executeMode('get')
                             .queryBy({hashKeyValue: codeID,
                                       sortKeyValue: workspaceID})
                             .execute();
        
        if (!code) {
            result.message = 'No code associated with code ' + codeID + ' in workspace ' + workspaceID;
        } else {
            let tags = code.attr('tags');
            if (!tags.find(t => t === tag)) {
                result.message = 'Tag "' + tag + '" not found on code ' + codeID + ' in workspace ' + workspaceID;
            } else {
                tags.splice(tags.indexOf(tag), 1);
                code.attr('tags', tags);
                code.update();
        
                result.success = true;
                result.message = 'Removed tag "' + tag + '" from code ' + codeID + ' in workspace ' + workspaceID;
            }
        }
    }    

    res.send(JSON.stringify(result));
};
