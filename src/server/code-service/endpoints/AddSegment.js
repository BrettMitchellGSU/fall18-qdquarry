
/* jshint ignore:start */

import { listMissing } from '../../../common/helpers';

import { Code } from '../../util/db/models/Code';

export const AddSegment = async function(req, res) {

    let {
        codeID,
        workspaceID,
        fileID,
        fileName,
        contents,
        offset
    } = req.body;

    let result = {success: false};

    let missing = listMissing({codeID, workspaceID, fileID, fileName, contents, offset});
    if (missing !== '') {
        result = {
            success: false,
            message: 'Missing parameters: ' + missing
        };
    } else {
        
        let code = await Code.get()
                             .executeMode('get')
                             .queryBy({hashKeyValue: codeID,
                                       sortKeyValue: workspaceID})
                             .execute();
        
        if (!code) {
            result.message = 'No code associated with code ' + codeID + ' in workspace ' + workspaceID;
        } else {
            let segments = code.attr('segments');
            segments.push({
                M: {
                    file_id: {S: fileID},
                    filename: {S: fileName},
                    segment_contents: {S: contents},
                    segment_length: {N: contents.length.toString()},
                    segment_offset: {N: offset.toString()}
                }
            });
            code.attr('segments', segments);
            code.update();
    
            result.success = true;
            result.message = 'Added segment to code ' + codeID + ' in workspace ' + workspaceID;
        }
    }

    res.send(JSON.stringify(result));
}
