
/* jshint ignore:start */

import { listMissing } from '../../../common/helpers';

import { Code } from '../../util/db/models/Code';

export const ChangeColor = async function(req, res) {

    let {
        codeID,
        workspaceID,
        color
    } = req.body;

    let result = {success: false};

    let missing = listMissing({codeID, workspaceID, color});
    if (missing !== '') {
        result = {
            success: false,
            message: 'Missing parameters: ' + missing
        };
    // The color string must be in hex format
    } else if (!(/^#?[0-9a-fA-F]{6}$/.test(color))) {
        result.message = 'Invalid color ' + color + '. Use one of these formats: #FFFFFF, #ffffff, FFFFFF, ffffff';
    } else {
        let code = await Code.get()
                             .executeMode('get')
                             .queryBy({hashKeyValue: codeID,
                                       sortKeyValue: workspaceID})
                             .execute();
        
        if (!code) {
            result.message = 'No code associated with code ' + codeID + ' in workspace ' + workspaceID;
        } else {
            code.attr('color', color.toUpperCase());
            code.update();

            result.success = true;
            result.message = 'Color changed to ' + color.toUpperCase() + ' for code ' + codeID + ' in workspace ' + workspaceID;
        }
    }

    res.send(JSON.stringify(result));
};
