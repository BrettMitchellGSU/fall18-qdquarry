
/* jshint ignore:start */

import { listMissing } from '../../../common/helpers';

import { Code } from '../../util/db/models/Code';

export const GetCode = async function(req, res) {

    let {
        codeID,
        workspaceID
    } = req.body;

    let result = {success: false};

    let missing = listMissing({codeID, workspaceID});
    if (missing !== '') {
        result = {
            success: false,
            message: 'Missing parameters: ' + missing
        };
    } else {
        let code = await Code.get()
                             .executeMode('get')
                             .queryBy({hashKeyValue: codeID,
                                       sortKeyValue: workspaceID})
                             .execute();
        
        if (!code) {
            result.message = 'No code associated with code ' + codeID + ' in workspace ' + workspaceID;
        } else {
            result.success = true;
            result.code = {
                color: code.attr('color'),
                segments: code.attr('segments'),
                tags: code.attr('tags')
            };
        }
    }

    res.send(JSON.stringify(result));
};
