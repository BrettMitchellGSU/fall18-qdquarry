
import { EndpointCollection } from '../util/EndpointCollection';

import { AddSegment    } from './endpoints/AddSegment';
import { RemoveSegment } from './endpoints/RemoveSegment';
import { AddTag        } from './endpoints/AddTag';
import { RemoveTag     } from './endpoints/RemoveTag';
import { ChangeColor   } from './endpoints/ChangeColor';
import { GetCode       } from './endpoints/GetCode';

export const endpoints = new EndpointCollection(
    {
        post: [
            [ '/a/code/add-segment',    AddSegment    ],
            [ '/a/code/remove-segment', RemoveSegment ],
            [ '/a/code/add-tag',        AddTag        ],
            [ '/a/code/remove-tag',     RemoveTag     ],
            [ '/a/code/change-color',   ChangeColor   ],
            [ '/a/code/get-code',       GetCode       ]
        ],

        get: []
    }
);
