const express = require('express');
const path = require('path');

let app = express();

app.get('/login', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/html/login.html'));
});
app.get('/change-password', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/html/change-password.html'));
});
app.get('/register', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/html/register.html'));
});
app.get('/dashboard', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/html/dashboard.html'));
});
app.get('/homepage', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/html/homepage.html'));
});
app.get('/workspace', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/html/workspace.html'));
});

app.get('/bundle/bundle.client.login.js', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../output/bundle/bundle.client.login.js'));
});
app.get('/bundle/bundle.client.login.css', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../output/bundle/bundle.client.login.css'));
});

app.get('/bundle/bundle.client.dashboard.js', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../output/bundle/bundle.client.dashboard.js'));
});
app.get('/bundle/bundle.client.dashboard.css', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../output/bundle/bundle.client.dashboard.css'));
});

app.get('/bundle/bundle.client.homepage.js', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../output/bundle/bundle.client.homepage.js'));
});
app.get('/bundle/bundle.client.homepage.css', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../output/bundle/bundle.client.homepage.css'));
});

app.get('/bundle/bundle.client.workspace.js', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../output/bundle/bundle.client.workspace.js'));
});
app.get('/bundle/bundle.client.workspace.css', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../output/bundle/bundle.client.workspace.css'));
});

app.get('/bundle/bundle.client.register.js', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../output/bundle/bundle.client.register.js'));
});
app.get('/bundle/bundle.client.register.css', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../output/bundle/bundle.client.register.css'));
});

app.get('/bundle/bundle.client.change-password.js', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../output/bundle/bundle.client.change-password.js'));
});
app.get('/bundle/bundle.client.change-password.css', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../../output/bundle/bundle.client.change-password.css'));
});

app.listen(8080);