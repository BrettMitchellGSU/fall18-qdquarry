
export const commaSeparatedList = function(vals, filter = (_ => true)) {
    var _filter = k => filter(vals[k]);

    return Object.keys(vals)
                 .filter(_filter)
                 .join(', ');
};

export const listNulls = function(vals) {
    return commaSeparatedList(vals, v => v === null);
};

export const listUndefined = function(vals) {
    return commaSeparatedList(vals, v => v === undefined);
};

export const listMissing = function(vals) {
    return commaSeparatedList(vals, v => v === null || v === undefined);
};

const hexChars = [..."0123456789ABCDEF"];
export const randomHex = function(len) {
    return [...Array(len)].map(
        v => hexChars[Math.floor(Math.random() * 16)]
    ).join('');
};

/**
 * Generates a new random hash key
 */
const hashKeyLength = 32;
export const newHashKey = function() {
    return randomHex(hashKeyLength);
};

const filePrefixLength = 16;
export const newFilePrefix = function() {
    return randomHex(filePrefixLength) + '_';
};
