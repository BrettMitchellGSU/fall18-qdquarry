
const run_shell_cmd = require('../run_shell_cmd');
// Start the server
run_shell_cmd({
    'win32': 'node .\\src\\server\\index.server.js',
    'linux': 'node ./src/server/index.server.js',
    'darwin': 'node ./src/server/index.server.js'
});