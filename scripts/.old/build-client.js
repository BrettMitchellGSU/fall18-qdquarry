
const run_shell_cmd = require('../run_shell_cmd');

// Get the configuration file to use for webpack
// This is passed in with the npm script
let arg = 'dev';

if (process.argv.length > 2)
    arg = process.argv[2];

if (arg === 'dev')
    arg = '--config webpack.config.client.dev.js';
else if (arg === 'dev-watch')
    arg = '--config webpack.config.client.dev.js --watch';
else if (arg === 'prod')
    arg = '--config webpack.config.client.prod.js';
else {
    console.log('Unknown build option ' + arg + '\n');
    process.exit();
}

console.log("Building client...\n");

// Run webpack with the given configuration file
run_shell_cmd({
    'win32': `.\\node_modules\\.bin\\webpack.cmd ${arg}`,
    'linux': `./node_modules/.bin/webpack ${arg}`,
    'darwin': `./node_modules/.bin/webpack ${arg}`
});