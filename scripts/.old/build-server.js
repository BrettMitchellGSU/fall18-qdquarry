
const run_shell_cmd = require('../run_shell_cmd');
const { readdirSync, statSync } = require('fs');
const { join, resolve } = require('path');

const getDirs = p => readdirSync(p)
                    .filter(f => statSync(join(p, f)).isDirectory());

let args = require('../get-args')();
let found_valid_arg = false;

if (args && args.length) {
    let dirs = getDirs(resolve(__dirname, '../src', 'server'));

    // Cycle through passed arguments 
    found_valid_arg = args.every((v) => {
        if (dirs.includes(v)) {
            console.log(`Building ${v}`);
            run_shell_cmd({
                'win32': `.\\node_modules\\.bin\\webpack.cmd --config .\\webpack.config.server.js --env.target=${v}`,
                'linux': `./node_modules/.bin/webpack.cmd --config ./webpack.config.server.js --env.target=${v}`,
                'darwin': `./node_modules/.bin/webpack.cmd --config ./webpack.config.server.js --env.target=${v}`
            });
            return false;
        }

        return true;
    });
}

if (!found_valid_arg)
    console.log('No build target specified. Aborting build process. Please re-run build-server with a microservice specified as a positional argument.');
