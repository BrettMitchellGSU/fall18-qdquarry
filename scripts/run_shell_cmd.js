const cp = require('child_process');
function puts(error, stdout, stderr) { console.log(stdout); };

/* Possible values for 'process.platform':
 *  - aix
 *  - darwin
 *  - freebsd
 *  - linux
 *  - openbsd
 *  - sunos
 *  - win32
 */

 /* Used to specify the proper shell to use for each platform
  *  - linux: /bin/bash
  *  - windows: process.env.ComSpec
  * 
  * NOTE: Only Linux and Windows are set at this point. This will not change the
  *       default shell in other operating systems
  */

let shells = {
    'win32': process.env.ComSpec,
    'linux': '/bin/bash',
    'darwin': '/bin/bash'
};

/**
 * Executes a command determined by the host OS
 * 
 * @function run_shell_cmd
 * @param {Map<String, String>} cmd_map A map from valid OS values of Node.js' process.platform attribute to commands specific to those OSs
 */
module.exports = function(cmd_map, spawn = false) {
    let executed = false;
    let exec = spawn ? cp.spawn : cp.execSync;

    for (let k in cmd_map) {
        if (process.platform === k && k !== 'default') {
            executed = true;
            return exec(cmd_map[k], {shell: shells[k], stdio: 'inherit'}, puts);
        }
    }

    // If the operating system was not found, and a default
    // command exists, execute the default command
    if (!executed && 'all' in cmd_map)
        return exec(cmd_map.all, puts);
};