
const run_shell_cmd = require('./run_shell_cmd');

// Test to see if init has been run before

// Create untracked folders
let dirs = './output ./output/docs ./output/dev ./output/prod ./output/coverage';
run_shell_cmd({
    'win32': 'mkdir ' + dirs.split('/').join('\\'),
    'linux': 'mkdir -p ' + dirs,
    'darwin': 'mkdir -p ' + dirs
});

// Install npm requirements
run_shell_cmd({
    'all': 'npm i'
});