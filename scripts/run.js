
let run_shell_cmd = require('./run_shell_cmd');

let args = require('./get-args')();
const targets = require('../conf/server-targets');

if (!args || !args[0] || !args[0].v) {
    console.error('Error: Please provide a run target: [all, a] or a server subtarget');
    process.exit(1);
}

let target = args[0].v;

if (target === 'all' || target === 'a') {

    let subProcs = [];

    targets.forEach(t => {
        subProcs.push(run_shell_cmd({
            'win32': `node .\\scripts\\run ${t.opts[0]}`,
            'linux': `node ./scripts/run ${t.opts[0]}`,
            'darwin': `node ./scripts/run ${t.opts[0]}`
        }, spawn = true));
    });

    console.log('Press enter to terminate all server processes\n' +
                '---------------------------------------------\n');

    process.stdin.on('data', () => {
        console.log('Stopping all server processes...');
        subProcs.forEach( p => p.kill('SIGINT') );
        console.log('All server processes stopped');
        process.exit(0);
    });

} else /* try for a server subtarget */ {

    let realTarget = null;

    targets.some( t => {
        if (t.opts.includes(target)) {
            realTarget = t.realTarget;
            return true; // found a target matching an argument
        }
        return false; // continue searching for a matching target
    });

    if (!realTarget) {
        console.error('Error: Please provide a run target. Use all or a configured server target specified in conf/server-targets.js.');
        process.exit(1);
    }

    let bundleName = '../output/bundle/bundle.server.' + realTarget + '.js';

    require(bundleName);

}
