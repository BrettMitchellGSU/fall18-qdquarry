
const run_shell_cmd = require('./run_shell_cmd');

run_shell_cmd({
    'win32': '.\\node_modules\\.bin\\jsdoc .\\src --recurse -c .\\jsdoc.conf.json -d .\\output/docs',
    'linux': './node_modules/.bin/jsdoc ./src --recurse -c ./jsdoc.conf.json -d ./output/docs',
    'darwin': './node_modules/.bin/jsdoc ./src --recurse -c ./jsdoc.conf.json -d ./output/docs'
});