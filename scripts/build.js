
let run_shell_cmd = require('./run_shell_cmd');

let args = require('./get-args')();

if (!args || !args[0] || !args[0].v) {
    console.error('Error: Please provide a build target: [client, c] or [server, s]');
    process.exit(1);
}

let target = args[0].v;

if (target === 'client' || target === 'c') {

    if (!args[1] || !args[1].v) {
        console.error('Error: Please provide a build subtarget. These are specified in conf/client-targets.js.');
        process.exit(1);
    }

    let subtarget = args[1].v;
    let mode = '';
    if (args[2] && args[2].v)
        mode = args[2].v;

    if (['p', 'prod', 'production'].includes(mode))
        mode = 'production';
    else
        mode = 'development';

    run_shell_cmd({
        'win32': `.\\node_modules\\.bin\\webpack.cmd --config .\\conf\\webpack.client.js --env.target=${subtarget} --env.mode=${mode}`,
        'linux': `./node_modules/.bin/webpack --config ./conf/webpack.client.js --env.target=${subtarget} --env.mode=${mode}`,
        'darwin': `./node_modules/.bin/webpack --config ./conf/webpack.client.js --env.target=${subtarget} --env.mode=${mode}`
    });

} else if (target === 'server' || target === 's') {

    if (!args[1] || !args[1].v) {
        console.error('Error: Please provide a build subtarget. These are specified in conf/server-targets.js.');
        process.exit(1);
    }

    let subtarget = args[1].v;
    let mode = '';
    if (args[2] && args[2].v)
        mode = args[2].v;

    if (['p', 'prod', 'production'].includes(mode))
        mode = 'production';
    else
        mode = 'development';

    run_shell_cmd({
        'win32': `.\\node_modules\\.bin\\webpack.cmd --config .\\conf\\webpack.server.js --env.target=${subtarget} --env.mode=${mode}`,
        'linux': `./node_modules/.bin/webpack --config ./conf/webpack.server.js --env.target=${subtarget} --env.mode=${mode}`,
        'darwin': `./node_modules/.bin/webpack --config ./conf/webpack.server.js --env.target=${subtarget} --env.mode=${mode}`
    });
}
