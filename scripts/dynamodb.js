const run_shell_cmd = require('./run_shell_cmd');

// Run dynamodb locally
run_shell_cmd({
    'win32': 'java -D"java.library.path=.\\lib\\DynamoDBLocal_lib" -jar .\\lib\\DynamoDBLocal.jar -sharedDb',
    'linux': 'java -Djava.library.path=./lib/DynamoDBLocal_lib -jar ./lib/DynamoDBLocal.jar -sharedDb',
    'darwin': 'java -Djava.library.path=./lib/DynamoDBLocal_lib -jar ./lib/DynamoDBLocal.jar -sharedDb'
});
