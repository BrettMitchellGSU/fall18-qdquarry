
module.exports = function getArgs() {
    let args = [];
    process.argv.forEach((v, i) => {
        if (i > 1) {
            let eq_idx = v.search('=');
            args.push(
                eq_idx === -1 ?
                    { v: v } :
                    { k: v.substr(0, eq_idx),
                      v: v.substr(eq_idx + 1) }
            );
        }
    });

    return args;
};