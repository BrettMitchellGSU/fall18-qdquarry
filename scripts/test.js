
const run_shell_cmd = require('./run_shell_cmd');

run_shell_cmd({
    'win32': '.\\node_modules\\.bin\\jest.cmd src',
    'linux': 'node_modules/.bin/jest ./src',
    'darwin': 'node_modules/.bin/jest ./src'
});