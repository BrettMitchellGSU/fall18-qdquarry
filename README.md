### QDQuarry Development Repository  

#### Frameworks installed:  
 - Node.js/Express.js: Server
 - DynamoDB: Database
 - React: Front-end development
 - Jest: JavaScript testing
 - Webpack: Front-end bundling
 - SCSS: Front-end styling
 - JSDoc: Documentation generation

[Download Node.js](https://nodejs.org/en/download/)

[DynamoDB Node.js Tutorial](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.NodeJs.html)  
[React Hello World Tutorial](https://reactjs.org/docs/hello-world.html)  
[Simple React Tutorial](https://reactjs.org/tutorial/tutorial.html)  

## Command reference:
 - **init**: A script that sets up all the untracked folders in the project. To
    reset, just delete output and node_modules and run again.
 - **build**: Single build script. Pass in either client or c to build the client,
    or server or s followed by a microservice subtarget to build a service.
    Example: node ./scripts/build server dynamodb
    This will build the dynamodb microservice and emit
    output/bundle/bundle.server.dynamodb-microservice.js
 - **run**: Single run script. Behaves exactly like the server functionality of the
    build script, and as such, you don't need to specify client or server.
    Example: node ./scripts/run dynamo
    This will run the bundled microservice without any other input from you.
    Ctrl-c to stop the service.
    This script has the added function: [all, a]. This option will run all
    currently registered services (specified in conf/server-targets.js) in
    subprocesses. Press enter to cancel this operation.
    Example: node ./scripts/run a
 - **dynamodb**: Runs a local instance of DynamoDB. This script may be necessary
    during the development of the DynamoDB microservice, but at some point it
    should become obselete.
 - **generate-docs**: Builds our documentation. Outputs to output/docs. Open
    output/docs/index.html in any web browser to view.
 - **test**: Runs all tests on all source, both server and client. This may change
    in the future to further streamline the dev process, but for now it works.