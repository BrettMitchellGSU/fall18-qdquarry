# Setting up your dev environment:

 - Run `pip install awscli --upgrade --user`. This will add the aws command line
   interface for you to access DynamoDB with
   - If you are on Linux or Mac, you can verify the installation with `which
     aws`
 - Run `node scripts/init.js`. This will perform all the preliminary operations necessary to start developing.

### Commands available in ./scripts/

 - build-front.js: Use this to transpile and bundle all the front-end code. This script can take 1 command line arguments:
   - build-front.js [dev|dev-watch|prod]
     - dev: Build the front-end for development purposes, but don't start the builder in watch mode
     - dev-watch: Build the front-end for development purposes in watch mode. With this option, Webpack observes changes to your files and updates the front-end on the fly so you can immediately see changes in your browser.
     - prod: Build a production bundle (obfuscated and minified)
 - generate-docs.js: Use this to update your local documentation. Since docs aren't stored in the repo, you do not need to commit when you update the docs.
 - init.js: Initializes the dev environment
 - test.js: Runs all our tests. [TODO: Add command line args to filter modules for tests]

 - run_shell_cmd.js: Used internally in the commands listed above. There probably won't be an occasion to use these.