# Documentation

We are using JSDoc for our documentation generation. This tool searches our  
source code for specifically formatted comments. A doc-string must be enclosed  
in `/** */`. `/* */`, even though it is a valid comment in JavaScript, will be  
ignored by JSDoc. Within the doc-string, descriptive tags starting with `@`  
are used to provide JSDoc with information on the element in question.  

##### Example
&nbsp;
```javascript
/**
 * @function concatStringAndNum concatStringAndNum takes a numeric value as its 
 *                              first parameter and a string as its second
 *                              parameter. It then concatenates the string value
 *                              of the first to the second and returns it.
 * @param {Number} param1 The number to concatenate with param2. This can be any
 *                        numeric format: integer, floating point, hexadecimal, 
 *                        or scientific notation
 * @param {String} param2 The string to concatenate with param1
 * @return {String} The result of concatenating the string value of param1 and 
 *                  param2
 */
function concatStringAndNum(param1, param2) {
    return param1.toString() + param2;
}
```

* * *

## Style guide

Every method needs to have a doc-string describing its purpose, parameters, and
return type

Format for free-standing methods:

```javascript
/**
 * @function funcName Function description
 * @param {Param1 Type} param1 Param1 description
 * @param {Param2 Type} param2 Param2 description
 * ...
 * @return {Return Type} Returned value description. Not necessary for void 
 *                       returns.
 */ 
```

